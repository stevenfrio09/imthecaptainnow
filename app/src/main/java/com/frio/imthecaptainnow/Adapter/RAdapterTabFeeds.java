package com.frio.imthecaptainnow.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frio.imthecaptainnow.GetterSetter.RParserTabFeeds;
import com.frio.imthecaptainnow.R;

import java.util.List;

/**
 * Created by steven on 12/13/17.
 */

public class RAdapterTabFeeds extends RecyclerView.Adapter<RAdapterTabFeeds.ViewHolder> {

    Context context;
    List<RParserTabFeeds> rParserTabFeedsList;

    public RAdapterTabFeeds(Context context, List<RParserTabFeeds> rParserTabFeedsList) {
        this.context = context;
        this.rParserTabFeedsList = rParserTabFeedsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerholder_tab_feeds, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RParserTabFeeds rParserTabFeeds = rParserTabFeedsList.get(position);

        holder.tv_feed_category.setText(rParserTabFeeds.getCategory().trim());
        holder.tv_feed_difficulty.setText(rParserTabFeeds.getDifficulty());
        holder.tv_feed_question.setText(rParserTabFeeds.getQuestion());
        holder.tv_feed_answer.setText(rParserTabFeeds.getAnswer_correct());

    }

    @Override
    public int getItemCount() {
        return rParserTabFeedsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_feed_category, tv_feed_difficulty, tv_feed_question, tv_feed_answer;
        CardView cv_feed_category_tag;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_feed_category = itemView.findViewById(R.id.tv_feed_category);
            tv_feed_difficulty = itemView.findViewById(R.id.tv_feed_difficulty);
            tv_feed_question = itemView.findViewById(R.id.tv_feed_question);
            tv_feed_answer = itemView.findViewById(R.id.tv_feed_answer);
            cv_feed_category_tag = itemView.findViewById(R.id.cv_feed_category_tag);

        }
    }
}
