package com.frio.imthecaptainnow.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frio.imthecaptainnow.GetterSetter.RParserCategories;
import com.frio.imthecaptainnow.R;

import java.util.List;

/**
 * Created by John Steven Frio on 2/1/2018.
 */

public class RAdapterCategories extends RecyclerView.Adapter<RAdapterCategories.ViewHolder> {

    Context context;
    List<RParserCategories> rParserCategoriesList;
    RAdapterCategoriesOnItemClick rAdapterCategoriesOnItemClick;

    public RAdapterCategories(Context context, List<RParserCategories> rParserCategoriesList, RAdapterCategoriesOnItemClick rAdapterCategoriesOnItemClick){

        this.context = context;
        this.rParserCategoriesList = rParserCategoriesList;
        this.rAdapterCategoriesOnItemClick = rAdapterCategoriesOnItemClick;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerholder_categories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final RParserCategories rParserCategories = rParserCategoriesList.get(position);

        holder.tv_category_id.setText(String.valueOf(rParserCategories.getId()));
        holder.tv_category.setText(rParserCategories.getCategory());
        holder.cv_recyclerCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAdapterCategoriesOnItemClick.onItemClick(rParserCategories.getId());
            }
        });

    }

    public interface RAdapterCategoriesOnItemClick{
        void onItemClick(int id);
    }

    @Override
    public int getItemCount() {
        return rParserCategoriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
     TextView tv_category, tv_category_id;
     CardView cv_recyclerCategories;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_category = itemView.findViewById(R.id.tv_category);
            tv_category_id = itemView.findViewById(R.id.tv_category_id);
            cv_recyclerCategories = itemView.findViewById(R.id.cv_recyclerCategories);

        }
    }
}
