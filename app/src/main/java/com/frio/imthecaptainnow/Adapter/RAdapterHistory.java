package com.frio.imthecaptainnow.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frio.imthecaptainnow.GetterSetter.RParserHistory;
import com.frio.imthecaptainnow.R;

import java.util.List;

/**
 * Created by steven on 1/16/18.
 */

public class RAdapterHistory extends RecyclerView.Adapter<RAdapterHistory.ViewHolder> {


    Context context;
    List<RParserHistory> rParserHistoryList;
    public RAdapterHistory(Context context, List<RParserHistory> rParserHistoryList){
        this.context = context;
        this.rParserHistoryList = rParserHistoryList;
    }

    @Override
    public RAdapterHistory.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerholder_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RAdapterHistory.ViewHolder holder, int position) {

        RParserHistory rParserHistory = rParserHistoryList.get(position);

        holder.tv_historyDate.setText(rParserHistory.getDate());
        holder.tv_historyNumAnswered.setText(String.valueOf(rParserHistory.getCorrect()));
        holder.tv_historyNumTotal.setText(String.valueOf(rParserHistory.getTotal()));


    }

    @Override
    public int getItemCount() {
        return rParserHistoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_historyDate, tv_historyNumAnswered, tv_historyNumTotal;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_historyDate = itemView.findViewById(R.id.tv_historyDate);
            tv_historyNumAnswered = itemView.findViewById(R.id.tv_historyNumAnswered);
            tv_historyNumTotal = itemView.findViewById(R.id.tv_historyNumTotal);

            //tv_historyPoints = itemView.findViewById(R.id.tv_historyPoints);


        }
    }
}
