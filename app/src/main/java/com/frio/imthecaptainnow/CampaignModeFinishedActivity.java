package com.frio.imthecaptainnow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CampaignModeFinishedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign_mode_finished);
    }
}
