package com.frio.imthecaptainnow;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.frio.imthecaptainnow.Config.Base64DecoderEncoder;
import com.frio.imthecaptainnow.Config.StringConfig;
import com.frio.imthecaptainnow.SQLite.CaptainSQLiteAdapter;
import com.frio.imthecaptainnow.SharedPreferencesManager.AnsweredQuestionsManager;
import com.frio.imthecaptainnow.SharedPreferencesManager.CampaignProgressManager;
import com.frio.imthecaptainnow.SharedPreferencesManager.SaveProgressManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends AppCompatActivity {

    Button btn_playCampaign, btn_triviaFeed, btn_playSandbox, btn_quit, btn_history, btn_playTimeAttack, btn_contribute;

    LinearLayout ll_homeActivity;

    Snackbar snackbar;

    Base64DecoderEncoder base64DecoderEncoder;
    CaptainSQLiteAdapter captainSQLiteAdapter;
    CaptainSQLiteAdapter.CaptainSQLite captainSQLite;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_home);




        //Log.d(StringConfig.LOG_TAG, "homeActivity:onCreate called");

        new CampaignProgressManager(HomeActivity.this).clearSharedPreferences();
        //deleteDatabase("CaptainSQLite.db");

        btn_playCampaign = findViewById(R.id.btn_playCampaign);
        btn_playSandbox = findViewById(R.id.btn_playSandbox);
        btn_triviaFeed = findViewById(R.id.btn_triviaFeed);
        btn_quit = findViewById(R.id.btn_quit);
        btn_history = findViewById(R.id.btn_history);
        btn_playTimeAttack = findViewById(R.id.btn_playTimeAttack);
        btn_contribute = findViewById(R.id.btn_contribute);

        ll_homeActivity = findViewById(R.id.ll_homeActivity);

        snackbar = Snackbar.make(ll_homeActivity, "Press back again to exit.", Snackbar.LENGTH_SHORT);

        base64DecoderEncoder = new Base64DecoderEncoder();
        captainSQLiteAdapter = new CaptainSQLiteAdapter(HomeActivity.this);
        captainSQLite = new CaptainSQLiteAdapter.CaptainSQLite(HomeActivity.this);

        captainSQLiteAdapter.deleteContents();

        requestQueue = Volley.newRequestQueue(HomeActivity.this);

        onClickListeners();

    }

    private void onClickListeners() {

        btn_playCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*DialogFragmentPlay dialogFragmentPlay = new DialogFragmentPlay();
                FragmentManager fragmentManager = getSupportFragmentManager();
                dialogFragmentPlay.show(fragmentManager, "tag");*/

                goToCampaignActivity();


            }
        });

        btn_triviaFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, TriviaFeedActivity.class));
            }
        });

        btn_playTimeAttack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuestions();
            }
        });

        btn_playSandbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuestions();
            }
        });

        btn_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, HistoryActivity.class));
            }
        });

        btn_contribute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ContributeQuestionsHomeActivity.class));
                finish();
            }
        });


    }

    private void goToCampaignActivity() {

        final SaveProgressManager saveProgressManager = new SaveProgressManager(this);

        if (saveProgressManager.isSharedPrefExisting()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Resume progress ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            SaveProgressManager saveProgressManager = new SaveProgressManager(HomeActivity.this);

                            String progress = saveProgressManager.getProgress();
                            int lives = saveProgressManager.getRemainingLives();

                            Log.d(StringConfig.LOG_TAG, "livelivelives : " + lives);

                            Bundle bundle = new Bundle();
                            bundle.putString("saveProgress", progress);
                            bundle.putInt("saveLives", lives);

                            Intent intent = new Intent(HomeActivity.this, CampaignProgressActivity.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            saveProgressManager.clearSharedPreferences();

                            startActivity(new Intent(HomeActivity.this, CampaignProgressActivity.class));
                            finish();
                        }
                    })
                    .show();
        } else {


            startActivity(new Intent(HomeActivity.this, CampaignProgressActivity.class));
            finish();
        }


    }

    private void getQuestions() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        final StringRequest stringRequest = new StringRequest(Request.Method.GET, StringConfig.OPENTDB_URL_RANDOM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int response_code = jsonObject.getInt("response_code");
                    switch (response_code) {
                        case 0:
                            JSONArray jsonObjectResults = jsonObject.getJSONArray("results");

                            for (int i = 0; i <= jsonObjectResults.length(); i++) {
                                JSONObject jsonObject1 = jsonObjectResults.getJSONObject(i);

                                String category = jsonObject1.getString("category");
                                String difficulty = jsonObject1.getString("difficulty");
                                String question = jsonObject1.getString("question");
                                String correct_answer = jsonObject1.getString("correct_answer");

                                JSONArray jsonArray = jsonObject1.getJSONArray("incorrect_answers");

                                String option1 = jsonArray.getString(0);
                                String option2 = jsonArray.getString(1);
                                String option3 = jsonArray.getString(2);

                                String decodedCategory = base64DecoderEncoder.base64Decoder(category);
                                String decodedDifficulty = base64DecoderEncoder.base64Decoder(difficulty);
                                String decodedQuestion = base64DecoderEncoder.base64Decoder(question);
                                String decodedAnswer = base64DecoderEncoder.base64Decoder(correct_answer);

                                String decodedOption1 = base64DecoderEncoder.base64Decoder(option1);
                                String decodedOption2 = base64DecoderEncoder.base64Decoder(option2);
                                String decodedOption3 = base64DecoderEncoder.base64Decoder(option3);

                                Log.d(StringConfig.LOG_TAG, "decodedCategory : " + decodedCategory);
                                Log.d(StringConfig.LOG_TAG, "decodedDifficulty : " + decodedDifficulty);
                                Log.d(StringConfig.LOG_TAG, "decodedQuestion : " + decodedQuestion);
                                Log.d(StringConfig.LOG_TAG, "decodedAnswer : " + decodedAnswer);

                                Log.d(StringConfig.LOG_TAG, "decodedOption1 : " + decodedOption1);
                                Log.d(StringConfig.LOG_TAG, "decodedOption2 : " + decodedOption2);
                                Log.d(StringConfig.LOG_TAG, "decodedOption3 : " + decodedOption3);

                                int dbID = i + 1;


                                long id = captainSQLiteAdapter.insertData(dbID, question, correct_answer, option1, option2, option3, category, difficulty, "false");

                                if (id >= 10) {
                                    progressDialog.dismiss();
                                    goToPlayActivity();
                                }
                            }
                            break;

                        case 1:
                            progressDialog.dismiss();
                            Toast.makeText(HomeActivity.this, "No available questions. Please choose a different category or difficulty.", Toast.LENGTH_SHORT).show();
                            break;
                        case 2:
                            progressDialog.dismiss();
                            Log.d(StringConfig.LOG_TAG, "response_code_2 : " + response_code);
                            break;
                        case 3:
                            progressDialog.dismiss();
                            Log.d(StringConfig.LOG_TAG, "response_code_3 : " + response_code);
                            break;

                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                displayToast("Couldn't receive questions from the server.");

            }
        });

        requestQueue.add(stringRequest);

    }

    private void goToPlayActivity() {

        //default value is 1,
        new AnsweredQuestionsManager(this).setAnsweredQuestions(1);

        Intent intent = new Intent(HomeActivity.this, SurvivalActivity.class);
        startActivity(intent);
        finish();
    }

    private void displayToast(String message) {
        Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Log.d(StringConfig.LOG_TAG, "homeActivity:onResume called");
        captainSQLiteAdapter.deleteContents();
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Log.d(StringConfig.LOG_TAG, "homeActivity:onPause called");

    }

    @Override
    public void onBackPressed() {
        if (snackbar.isShown()) {
            super.onBackPressed();
        } else {
            snackbar.show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
