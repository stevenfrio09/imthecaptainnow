package com.frio.imthecaptainnow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frio.imthecaptainnow.Config.StringConfig;
import com.frio.imthecaptainnow.SQLite.HistorySQLiteAdapter;
import com.frio.imthecaptainnow.SharedPreferencesManager.PointingManager;
import com.frio.imthecaptainnow.SharedPreferencesManager.SaveProgressManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GameResultActivity extends AppCompatActivity {

    LinearLayout ll_gameResult;
    TextView tv_resultCorrect, tv_resultTotal;

    Snackbar snackbar;

    PointingManager pointingManager;

    HistorySQLiteAdapter historySQLiteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_game_result);

        ll_gameResult = findViewById(R.id.ll_gameResult);
        tv_resultCorrect = findViewById(R.id.tv_resultCorrect);
        tv_resultTotal = findViewById(R.id.tv_resultTotal);

        snackbar = Snackbar.make(ll_gameResult, "Press back again to return to main menu.", Snackbar.LENGTH_SHORT);

        pointingManager = new PointingManager(this);

        historySQLiteAdapter = new HistorySQLiteAdapter(this);

        Bundle bundle = new Bundle();
        if (bundle != null) {
            int points = bundle.getInt("points");
            Log.d(StringConfig.LOG_TAG, "bundleValue : " + points);
        }

        int numCorrect = pointingManager.getCorrect();
        int total = Integer.parseInt(pointingManager.getTotal());

        tv_resultCorrect.setText(String.valueOf(numCorrect));
        tv_resultTotal.setText(String.valueOf(total));

        historySQLiteAdapter.insertData(numCorrect, total, getCurrentDateAndTime());


    }

    @Override
    public void onBackPressed() {

        if (snackbar.isShown()) {

            new SaveProgressManager(this).clearSharedPreferences();

            startActivity(new Intent(GameResultActivity.this, HomeActivity.class));
            finish();
        } else {
            snackbar.show();
        }
    }

    public String getCurrentDateAndTime() {


        String dateFormat = new SimpleDateFormat("MMM d yyyy HH:mm", Locale.getDefault()).format(new Date());
        Log.d(StringConfig.LOG_TAG, "dateFormat : " + dateFormat);

        return dateFormat;

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
