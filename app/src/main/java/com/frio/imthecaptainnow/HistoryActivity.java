package com.frio.imthecaptainnow;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.frio.imthecaptainnow.Adapter.RAdapterHistory;
import com.frio.imthecaptainnow.GetterSetter.RParserHistory;
import com.frio.imthecaptainnow.SQLite.HistorySQLiteAdapter;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HistoryActivity extends AppCompatActivity {

    RecyclerView rv_history;
    List<RParserHistory> rParserHistoryList;
    RAdapterHistory rAdapterHistory;
    LinearLayoutManager linearLayoutManager;

    HistorySQLiteAdapter historySQLiteAdapter;
    HistorySQLiteAdapter.HistorySQLite historySQLite;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_history);

        rv_history = findViewById(R.id.rv_history);

        rParserHistoryList = new ArrayList<>();
        rAdapterHistory = new RAdapterHistory(this, rParserHistoryList);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        historySQLiteAdapter = new HistorySQLiteAdapter(this);
        historySQLite = new HistorySQLiteAdapter.HistorySQLite(this);

        sqLiteDatabase = historySQLite.getWritableDatabase();


        displayHistoryFromDb();

    }

    public void displayHistoryFromDb(){

        String readQuery = "SELECT * FROM "+HistorySQLiteAdapter.HistorySQLite.TABLE_NAME+"";

        String date;
        int correct, total;

        Cursor cursor = sqLiteDatabase.rawQuery(readQuery, null);
        while(cursor.moveToNext()){
            date = cursor.getString(cursor.getColumnIndex(HistorySQLiteAdapter.HistorySQLite.COL_DATE));
            correct = cursor.getInt(cursor.getColumnIndex(HistorySQLiteAdapter.HistorySQLite.COL_CORRECT));
            total = cursor.getInt(cursor.getColumnIndex(HistorySQLiteAdapter.HistorySQLite.COL_TOTAL));

            RParserHistory rParserHistory = new RParserHistory(date, correct, total);
            rParserHistoryList.add(rParserHistory);

        }

        rv_history.setAdapter(rAdapterHistory);
        rv_history.setLayoutManager(linearLayoutManager);
        rAdapterHistory.notifyDataSetChanged();

        cursor.close();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
