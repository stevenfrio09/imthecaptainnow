package com.frio.imthecaptainnow;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.frio.imthecaptainnow.Config.Base64DecoderEncoder;
import com.frio.imthecaptainnow.Config.ShuffleChoices;
import com.frio.imthecaptainnow.Config.StringConfig;
import com.frio.imthecaptainnow.GameConfig.AnimateButton;
import com.frio.imthecaptainnow.SQLite.CaptainSQLiteAdapter;
import com.frio.imthecaptainnow.SharedPreferencesManager.AnsweredQuestionsManager;
import com.frio.imthecaptainnow.SharedPreferencesManager.CampaignProgressManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CampaignMainActivity extends AppCompatActivity {

    ProgressBar pb_campaignTimeRemaining;
    Button btn_campaignOption1, btn_campaignOption2, btn_campaignOption3, btn_campaignOption4;

    ImageView iv_campaignLife01, iv_campaignLife02, iv_campaignLife03, iv_campaignLife04, iv_campaignLife05;

    TextView tv_campaignQuestion, tv_toolbarTitle;

    Toolbar tb_campaignMain;

    CountDownTimer countDownTimer;

    CampaignProgressManager campaignProgressManager;
    AnsweredQuestionsManager answeredQuestionsManager;

    CaptainSQLiteAdapter captainSQLiteAdapter;

    CaptainSQLiteAdapter.CaptainSQLite captainSQLite;
    Base64DecoderEncoder base64DecoderEncoder;

    AnimateButton animateButton;

    int numQuestions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_campaign_main);

        //Log.d(StringConfig.LOG_TAG, "campaignMainActivity:onCreate called");

        pb_campaignTimeRemaining = findViewById(R.id.pb_campaignTimeRemaining);
        btn_campaignOption1 = findViewById(R.id.btn_campaignOption1);
        btn_campaignOption2 = findViewById(R.id.btn_campaignOption2);
        btn_campaignOption3 = findViewById(R.id.btn_campaignOption3);
        btn_campaignOption4 = findViewById(R.id.btn_campaignOption4);

        iv_campaignLife01 = findViewById(R.id.iv_campaignLife01);
        iv_campaignLife02 = findViewById(R.id.iv_campaignLife02);
        iv_campaignLife03 = findViewById(R.id.iv_campaignLife03);
        iv_campaignLife04 = findViewById(R.id.iv_campaignLife04);
        iv_campaignLife05 = findViewById(R.id.iv_campaignLife05);

        tv_campaignQuestion = findViewById(R.id.tv_campaignQuestion);
        tv_toolbarTitle = findViewById(R.id.tv_toolbarTitle);

        tb_campaignMain = findViewById(R.id.tb_campaignMain);


        campaignProgressManager = new CampaignProgressManager(this);

        answeredQuestionsManager = new AnsweredQuestionsManager(this);

        captainSQLiteAdapter = new CaptainSQLiteAdapter(this);

        base64DecoderEncoder = new Base64DecoderEncoder();

        animateButton = new AnimateButton(this);

        Bundle bundle = getIntent().getExtras();
        String strCategory = bundle.getString("category");
        Log.d(StringConfig.LOG_TAG, "strCategory : " + strCategory);
        numQuestions = bundle.getInt("numQuestions");

        int questionNumber = answeredQuestionsManager.getAnsweredQuestions();


        displayQA(questionNumber);

        tv_toolbarTitle.setText(strCategory);

        countdownTimerCancel();
        countdownTimerStart();

        onClickListeners(btn_campaignOption1);
        onClickListeners(btn_campaignOption2);
        onClickListeners(btn_campaignOption3);
        onClickListeners(btn_campaignOption4);

        manageLives(true);

    }


    private void displayQA(int questionNumber) {

        //Log.d(StringConfig.LOG_TAG, "displayQA called");

        int i = questionNumber;
        while (captainSQLiteAdapter.selectIsAnswered(i).equalsIgnoreCase("false")) {

            //Log.d(StringConfig.LOG_TAG, "iValue : " + i);

            String question = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectQuestion(i));
            String option1 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption1(i));
            String option2 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption2(i));
            String option3 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption3(i));
            String option4 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption4(i));
            String difficulty = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectDifficulty(i));
            String category = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectCategory(i));
            String isAnswered = captainSQLiteAdapter.selectIsAnswered(i);

            new ShuffleChoices(CampaignMainActivity.this).getChoicesFromDB(i, btn_campaignOption1, btn_campaignOption2, btn_campaignOption3, btn_campaignOption4);

            /*Log.d(StringConfig.LOG_TAG, "question decrypt : " + captainSQLiteAdapter.selectQuestion(i));
            Log.d(StringConfig.LOG_TAG, "question : " + question);
            Log.d(StringConfig.LOG_TAG, "option1 : " + option1);
            Log.d(StringConfig.LOG_TAG, "option2 : " + option2);
            Log.d(StringConfig.LOG_TAG, "option3 : " + option3);
            Log.d(StringConfig.LOG_TAG, "option4 : " + option4);
            Log.d(StringConfig.LOG_TAG, "difficulty : " + difficulty);
            Log.d(StringConfig.LOG_TAG, "category : " + category);
            Log.d(StringConfig.LOG_TAG, "isAnswered : " + isAnswered);*/

            tv_campaignQuestion.setText(question);

            break;
        }

    }

    private void initNextQuestion() {

        btn_campaignOption1.setEnabled(true);
        btn_campaignOption2.setEnabled(true);
        btn_campaignOption3.setEnabled(true);
        btn_campaignOption4.setEnabled(true);

        animateButton.defaultButton(btn_campaignOption1, btn_campaignOption2, btn_campaignOption3, btn_campaignOption4);

        Log.d(StringConfig.LOG_TAG, "spCampaignProgress : " + campaignProgressManager.getCampaignProgress());
        Log.d(StringConfig.LOG_TAG, "spNumQuestions : " + numQuestions);
        Log.d(StringConfig.LOG_TAG, "spLivesLeft : " + campaignProgressManager.getCampaignLives());
        Log.d(StringConfig.LOG_TAG, "spAnsweredQuestions : " + campaignProgressManager.getAnsweredQuestions());

        if(Integer.parseInt(campaignProgressManager.getCampaignProgress()) >= 6 && campaignProgressManager.getAnsweredQuestions() == numQuestions){

            countdownTimerCancel();
            startActivity(new Intent(CampaignMainActivity.this, CampaignModeFinishedActivity.class));
            finish();

        }else {

            if (campaignProgressManager.getCampaignLives() >= 0) {

                if (campaignProgressManager.getAnsweredQuestions() >= numQuestions) {

                    Bundle bundle = new Bundle();
                    bundle.putInt("livesLeft", campaignProgressManager.getCampaignLives());

                    countdownTimerCancel();

                    Intent intent = new Intent(CampaignMainActivity.this, CampaignProgressActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                    //Toast.makeText(CampaignMainActivity.this, "numQuestions : " + numQuestions, Toast.LENGTH_SHORT).show();

                } else {

                    int answeredQuestionNumber = answeredQuestionsManager.getAnsweredQuestions() + 1;
                    answeredQuestionsManager.setAnsweredQuestions(answeredQuestionNumber);

                    campaignProgressManager.setAnsweredQuestions(campaignProgressManager.getAnsweredQuestions() + 1);

                    Log.d(StringConfig.LOG_TAG, "setAnsweredQuestions : " + answeredQuestionsManager.getAnsweredQuestions());

                    //updates isAnswered column in db, writes true if the question is answered
                    captainSQLiteAdapter.updateIsAnswered(answeredQuestionsManager.getAnsweredQuestions() - 1);

                    displayQA(answeredQuestionNumber);

                    countdownTimerCancel();
                    countdownTimerStart();

                }
            } else {

                countdownTimerCancel();
                Toast.makeText(CampaignMainActivity.this, "Game Over !", Toast.LENGTH_SHORT).show();


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        startActivity(new Intent(CampaignMainActivity.this, CampaignModeFinishedActivity.class));

                    }
                }, 500);

            }
        }


    }

    //initialize options
    private void onClickListeners(final Button button) {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String question = tv_campaignQuestion.getText().toString();
                String encodedQuestion = base64DecoderEncoder.base64Encoder(question);

                String correctAnswer = captainSQLiteAdapter.selectOption1(answeredQuestionsManager.getAnsweredQuestions());
                String decodedAnswer = base64DecoderEncoder.base64Decoder(correctAnswer);
                String selectedAnswer = button.getText().toString();

                /*Log.d(StringConfig.LOG_TAG, "encodedQuestion : " + encodedQuestion);
                Log.d(StringConfig.LOG_TAG, "decodedAnswer : " + decodedAnswer);
                Log.d(StringConfig.LOG_TAG, "selectedAnswer : " + selectedAnswer);*/

                //disable button to prevent double clicks
                btn_campaignOption1.setEnabled(false);
                btn_campaignOption2.setEnabled(false);
                btn_campaignOption3.setEnabled(false);
                btn_campaignOption4.setEnabled(false);

                if (answeredQuestionsManager.getAnsweredQuestions() > numQuestions) {

                    //tv_playSolo_timeLeft.setText(String.valueOf(0));

                    countdownTimerCancel();

                    //submitScore();

                } else {

                    if (decodedAnswer.equals(selectedAnswer)) {

                        //Toast.makeText(PlaySoloActivity.this, "Answer is correct : " + decodedAnswer + " : " + selectedAnswer, Toast.LENGTH_SHORT).show();

                        //calculatePoints();

                    } else {
                        //Toast.makeText(PlaySoloActivity.this, "Answer is wrong : " + decodedAnswer + " : " + selectedAnswer, Toast.LENGTH_SHORT).show();

                        animateButton.wrongAnswer(button);

                        manageLives(false);

                    }

                    animateButton.correctAnswer(decodedAnswer, btn_campaignOption1, btn_campaignOption2, btn_campaignOption3, btn_campaignOption4);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            countdownTimerCancel();
                            countdownTimerStart();
                            initNextQuestion();
                        }
                    }, 1200);

                }

            }
        });
    }


    private void manageLives(boolean isRight) {

        int currentLives;

        if (!isRight) {

            currentLives = campaignProgressManager.getCampaignLives() -1;

        } else {
            currentLives = campaignProgressManager.getCampaignLives();
        }

        Log.d(StringConfig.LOG_TAG, "currentLives : " + currentLives);

        campaignProgressManager.setCampaignLives(currentLives);

        switch (currentLives) {

            case 5:
                iv_campaignLife05.setVisibility(View.VISIBLE);
                iv_campaignLife04.setVisibility(View.VISIBLE);
                iv_campaignLife03.setVisibility(View.VISIBLE);
                iv_campaignLife02.setVisibility(View.VISIBLE);
                iv_campaignLife01.setVisibility(View.VISIBLE);
                break;
            case 4:
                iv_campaignLife05.setVisibility(View.INVISIBLE);
                break;
            case 3:
                iv_campaignLife05.setVisibility(View.INVISIBLE);
                iv_campaignLife04.setVisibility(View.INVISIBLE);
                break;
            case 2:
                iv_campaignLife05.setVisibility(View.INVISIBLE);
                iv_campaignLife04.setVisibility(View.INVISIBLE);
                iv_campaignLife03.setVisibility(View.INVISIBLE);
                break;
            case 1:
                iv_campaignLife05.setVisibility(View.INVISIBLE);
                iv_campaignLife04.setVisibility(View.INVISIBLE);
                iv_campaignLife03.setVisibility(View.INVISIBLE);
                iv_campaignLife02.setVisibility(View.INVISIBLE);
                break;
            case 0:
                iv_campaignLife05.setVisibility(View.INVISIBLE);
                iv_campaignLife04.setVisibility(View.INVISIBLE);
                iv_campaignLife03.setVisibility(View.INVISIBLE);
                iv_campaignLife02.setVisibility(View.INVISIBLE);
                iv_campaignLife01.setVisibility(View.INVISIBLE);
                break;
        }

    }

    private void countdownTimerStart() {

        Log.d(StringConfig.LOG_TAG, "countdownTimerStart:called");

        countDownTimer = new CountDownTimer(15000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int timeLeft = (int) (millisUntilFinished / 1000);
                Log.d(StringConfig.LOG_TAG, "timeLeft : " + timeLeft);
                pb_campaignTimeRemaining.setProgress(timeLeft);

                if(timeLeft <= 15 && timeLeft >= 11){

                    //pb_campaignTimeRemaining.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_campaign_green));

                }else if(timeLeft <= 10 && timeLeft >= 6){

                    //pb_campaignTimeRemaining.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_campaign_yellow));

                }else {

                    //pb_campaignTimeRemaining.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_campaign_red));

                }


            }

            @Override
            public void onFinish() {
                pb_campaignTimeRemaining.setProgress(0);

                String correctAnswer = captainSQLiteAdapter.selectOption1(answeredQuestionsManager.getAnsweredQuestions());
                String decodedAnswer = base64DecoderEncoder.base64Decoder(correctAnswer);

                Log.d(StringConfig.LOG_TAG, "answeredQuestion : " + answeredQuestionsManager.getAnsweredQuestions());
                Log.d(StringConfig.LOG_TAG, "campaignProgress : " + campaignProgressManager.getAnsweredQuestions());

                Log.d(StringConfig.LOG_TAG, "onFinish:answeredQuestionManager : " + answeredQuestionsManager.getAnsweredQuestions());
                Log.d(StringConfig.LOG_TAG, "onFinish:spAnsweredQuestion : " + campaignProgressManager.getAnsweredQuestions());

                if (answeredQuestionsManager.getAnsweredQuestions() == campaignProgressManager.getAnsweredQuestions()) {
                    manageLives(false);
                }

                animateButton.correctAnswer(decodedAnswer, btn_campaignOption1, btn_campaignOption2, btn_campaignOption3, btn_campaignOption4);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initNextQuestion();
                    }
                }, 1200);


            }
        }.start();
    }

    private void countdownTimerCancel() {


        Log.d(StringConfig.LOG_TAG, "countdownTimerCancel:called");
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Surrender ?")
                .setMessage("Are you sure captain ? Your crew needs you !")
                .setPositiveButton("Aye, aye !", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteDatabase("CaptainSQLite.db");
                        startActivity(new Intent(CampaignMainActivity.this, HomeActivity.class));
                        finish();

                    }
                })
                .setNegativeButton("Noooooo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();

    }

/*
    @Override
    protected void onResume() {
        super.onResume();
        //Log.d(StringConfig.LOG_TAG, "campaignMainActivity:onResume called");

    }*/

    /*@Override
    protected void onPause() {
        super.onPause();
        countdownTimerCancel();
        Log.d(StringConfig.LOG_TAG, "campaignMainActivity:onPause called");

    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countdownTimerCancel();
        Log.d(StringConfig.LOG_TAG, "campaignMainActivity:onDestroy called");
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
