package com.frio.imthecaptainnow.GameConfig;

import android.content.Context;
import android.os.Handler;
import android.widget.Button;

import com.frio.imthecaptainnow.R;

/**
 * Created by steven on 1/19/18.
 */

public class AnimateButton {
    Context context;

    public AnimateButton(Context context){
        this.context = context;
    }

    public void correctAnswer(final String correctAnswer, final Button btn_option1, final Button btn_option2, final Button btn_option3, final Button btn_option4){
        final String button1 = btn_option1.getText().toString();
        final String button2 = btn_option2.getText().toString();
        final String button3 = btn_option3.getText().toString();
        final String button4 = btn_option4.getText().toString();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (button1.equals(correctAnswer)) {
                    btn_option1.setBackgroundResource(R.drawable.btn_campaign_main_top_correct);
                }

                if (button2.equals(correctAnswer)) {
                    btn_option2.setBackgroundResource(R.drawable.btn_campaign_main_mid_correct);
                }

                if (button3.equals(correctAnswer)) {
                    btn_option3.setBackgroundResource(R.drawable.btn_campaign_main_mid_correct);
                }

                if (button4.equals(correctAnswer)) {
                    btn_option4.setBackgroundResource(R.drawable.btn_campaign_main_bottom_correct);
                }


            }
        }, 200);
    }


    public void wrongAnswer(final Button button) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                button.setBackgroundColor(context.getResources().getColor(R.color.deep_orange_500));

            }
        }, 250);
    }

    public void defaultButton(final Button btn_option1, final Button btn_option2, final Button btn_option3, final Button btn_option4){

        btn_option1.setBackgroundResource(R.drawable.btn_campaign_main_top_default);
        btn_option2.setBackgroundResource(R.drawable.btn_campaign_main_mid_default);
        btn_option3.setBackgroundResource(R.drawable.btn_campaign_main_mid_default);
        btn_option4.setBackgroundResource(R.drawable.btn_campaign_main_bottom_default);

    }

    public void defaultButtonsSurvival(final Button btn_option1, final Button btn_option2, final Button btn_option3, final Button btn_option4){

        btn_option1.setBackgroundResource(R.drawable.btn_campaign_timeattack_top_default);
        btn_option2.setBackgroundResource(R.drawable.btn_campaign_timeattack_mid_default);
        btn_option3.setBackgroundResource(R.drawable.btn_campaign_timeattack_mid_default);
        btn_option4.setBackgroundResource(R.drawable.btn_campaign_timeattack_bottom_default);

    }

}
