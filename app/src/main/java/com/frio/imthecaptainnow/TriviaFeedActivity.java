package com.frio.imthecaptainnow;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.frio.imthecaptainnow.Adapter.RAdapterCategories;
import com.frio.imthecaptainnow.Adapter.RAdapterTabFeeds;
import com.frio.imthecaptainnow.Config.Base64DecoderEncoder;
import com.frio.imthecaptainnow.Config.StringConfig;
import com.frio.imthecaptainnow.GetterSetter.RParserCategories;
import com.frio.imthecaptainnow.GetterSetter.RParserTabFeeds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TriviaFeedActivity extends AppCompatActivity {


    SwipeRefreshLayout srl_feeds;
    RecyclerView rv_feeds, rv_feed_category;

    List<RParserTabFeeds> rParserFeedsList;
    RAdapterTabFeeds rAdapterFeeds;
    LinearLayoutManager linearLayoutManager;
    Base64DecoderEncoder base64DecoderEncoder;

    List<RParserCategories> rParserCategoriesList;
    RAdapterCategories rAdapterCategories;
    StaggeredGridLayoutManager staggeredGridLayoutManager;

    Snackbar snackbar;

    FrameLayout nestedScrollView;
    BottomSheetBehavior bottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_trivia_feed);

        rv_feeds = findViewById(R.id.rv_tab_feeds);
        rv_feed_category = findViewById(R.id.rv_feed_category);
        srl_feeds = findViewById(R.id.srl_tab_feeds);

        rParserFeedsList = new ArrayList<>();
        rParserCategoriesList = new ArrayList<>();


        base64DecoderEncoder = new Base64DecoderEncoder();

        snackbar = Snackbar.make(srl_feeds, "Press back again to exit.", Snackbar.LENGTH_SHORT);

        nestedScrollView = findViewById(R.id.cv_bottomSheet);
        bottomSheetBehavior = BottomSheetBehavior.from(nestedScrollView);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        eventListeners();

        initReyclerViewsTriviaFeed(false, 0);
        initReyclerViewsCategories();

    }

    private void eventListeners() {

        srl_feeds.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                initReyclerViewsTriviaFeed(false, 0);

            }
        });
    }

    private void initReyclerViewsTriviaFeed(boolean isFiltered, int category) {

        rParserFeedsList.clear();
        volleyRequestTriviaFeed(isFiltered, category);
        rAdapterFeeds = new RAdapterTabFeeds(this, rParserFeedsList);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_feeds.setLayoutManager(linearLayoutManager);
        rv_feeds.setAdapter(rAdapterFeeds);

    }

    private void initReyclerViewsCategories() {

        rParserCategoriesList.clear();
        volleyRequestCategories();
        rAdapterCategories = new RAdapterCategories(this, rParserCategoriesList, new RAdapterCategories.RAdapterCategoriesOnItemClick() {
            @Override
            public void onItemClick(int id) {
                initReyclerViewsTriviaFeed(true, id);
            }
        });
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        rv_feed_category.setLayoutManager(staggeredGridLayoutManager);
        rv_feed_category.setAdapter(rAdapterCategories);

    }

    private void volleyRequestTriviaFeed(boolean isFiltered, int category) {

        srl_feeds.setRefreshing(true);

        String url = "";

        if (isFiltered) {
            url = StringConfig.OPENTDB_URL_TRIVIA_FEED_FILTER_CATEGORY(category);
        } else {
            url = StringConfig.OPENTDB_URL_TRIVIA_FEED;
        }

        RequestQueue requestQueue = Volley.newRequestQueue(this);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonObjectResults = jsonObject.getJSONArray("results");

                    for (int i = 0; i <= jsonObjectResults.length(); i++) {

                        JSONObject jsonObject1 = jsonObjectResults.getJSONObject(i);

                        String question = jsonObject1.getString("question");
                        String category = jsonObject1.getString("category");
                        String difficulty = jsonObject1.getString("difficulty");
                        String correct_answer = jsonObject1.getString("correct_answer");

                        String decodedCategory = base64DecoderEncoder.base64Decoder(category);
                        String decodedDifficulty = base64DecoderEncoder.base64Decoder(difficulty);
                        String decodedQuestion = base64DecoderEncoder.base64Decoder(question);
                        String decodedCorrectAnswer = base64DecoderEncoder.base64Decoder(correct_answer);

                        Log.d(StringConfig.LOG_TAG, "category : " + decodedCategory);
                        Log.d(StringConfig.LOG_TAG, "difficulty : " + decodedDifficulty);
                        Log.d(StringConfig.LOG_TAG, "question : " + decodedQuestion);
                        Log.d(StringConfig.LOG_TAG, "correct_answer : " + decodedCorrectAnswer);

                        RParserTabFeeds rParserTabFeeds = new RParserTabFeeds(decodedCategory, decodedDifficulty, decodedQuestion, decodedCorrectAnswer);
                        rParserFeedsList.add(rParserTabFeeds);
                        rAdapterFeeds.notifyDataSetChanged();

                        srl_feeds.setRefreshing(false);

                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(StringConfig.LOG_TAG, "errorResponse called : " + error.toString());
                srl_feeds.setRefreshing(false);
                Toast.makeText(TriviaFeedActivity.this, "An error has occured.", Toast.LENGTH_SHORT);
            }
        });

        requestQueue.add(stringRequest);


    }


    private void volleyRequestCategories() {

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //String urlTriviaFeed = "https://opentdb.com/api.php?amount=10&encode=base64";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, StringConfig.OPENTDB_URL_RETRIEVE_CATEGORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String categories = jsonObject.getString("trivia_categories");

                    JSONArray jsonArray = new JSONArray(categories);
                    for (int i = 0; i <= jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        int id = jsonObject1.getInt("id");
                        String name = jsonObject1.getString("name");


                        RParserCategories rParserCategories = new RParserCategories(id, name);
                        rParserCategoriesList.add(rParserCategories);
                        rAdapterCategories.notifyDataSetChanged();

                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(StringConfig.LOG_TAG, "errorResponse called : " + error.toString());

                Toast.makeText(TriviaFeedActivity.this, "An error has occured.", Toast.LENGTH_SHORT);
            }
        });

        requestQueue.add(stringRequest);


    }


    @Override
    public void onBackPressed() {
       /* if(snackbar.isShown()){
            super.onBackPressed();
        }else{
            snackbar.show();
        }*/
        super.onBackPressed();
    }

    public void filterCategories(View view) {

        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        } else {

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
