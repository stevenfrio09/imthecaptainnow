package com.frio.imthecaptainnow;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.frio.imthecaptainnow.Config.Base64DecoderEncoder;
import com.frio.imthecaptainnow.Config.ShuffleChoices;
import com.frio.imthecaptainnow.Config.StringConfig;
import com.frio.imthecaptainnow.GameConfig.AnimateButton;
import com.frio.imthecaptainnow.SQLite.CaptainSQLiteAdapter;
import com.frio.imthecaptainnow.SharedPreferencesManager.AnsweredQuestionsManager;
import com.frio.imthecaptainnow.SharedPreferencesManager.PointingManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SurvivalActivity extends AppCompatActivity {

    RequestQueue requestQueue;

    TextView tv_playSolo_question, tv_playSolo_question_number, tv_playSolo_category, tv_playSolo_timeLeft;

    Button btn_playSolo_option1, btn_playSolo_option2, btn_playSolo_option3, btn_playSolo_option4;

    //ProgressBar pb_playSolo;

    Base64DecoderEncoder base64DecoderEncoder;

    CaptainSQLiteAdapter captainSQLiteAdapter;
    CaptainSQLiteAdapter.CaptainSQLite captainSQLite;

    AnsweredQuestionsManager answeredQuestionsManager;

    AnimateButton animateButton;

    PointingManager pointingManager;

    CountDownTimer countDownTimer;

    DateTime dateTime;

    Toolbar tb;
    RelativeLayout rl_survival;
    CardView cv_playSolo_question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_survival);



        tv_playSolo_question = findViewById(R.id.tv_playSolo_question);
        tv_playSolo_question_number = findViewById(R.id.tv_playSolo_question_number);
        tv_playSolo_category = findViewById(R.id.tv_playSolo_category);
        tv_playSolo_timeLeft = findViewById(R.id.tv_playSolo_timeLeft);

        btn_playSolo_option1 = findViewById(R.id.btn_playSolo_option1);
        btn_playSolo_option2 = findViewById(R.id.btn_playSolo_option2);
        btn_playSolo_option3 = findViewById(R.id.btn_playSolo_option3);
        btn_playSolo_option4 = findViewById(R.id.btn_playSolo_option4);

        //pb_playSolo = findViewById(R.id.pb_playSolo);

        requestQueue = Volley.newRequestQueue(this);

        base64DecoderEncoder = new Base64DecoderEncoder();

        captainSQLiteAdapter = new CaptainSQLiteAdapter(this);

        answeredQuestionsManager = new AnsweredQuestionsManager(this);

        animateButton = new AnimateButton(this);

        pointingManager = new PointingManager(this);

        dateTime = new DateTime();

        tb = findViewById(R.id.tb);

        rl_survival = findViewById(R.id.rl_survival);

        cv_playSolo_question = findViewById(R.id.cv_playSolo_question);




        int questionNumber = answeredQuestionsManager.getAnsweredQuestions();

        pointingManager.clearSharedPrefPointing();

        pointingManager.setCorrect(0);
        pointingManager.setTotal(String.valueOf(0));

        tv_playSolo_question_number.setText("1");

        displayQA(questionNumber);

        countdownTimerCancel();
        countdownTimerStart();

        onClickListeners(btn_playSolo_option1);
        onClickListeners(btn_playSolo_option2);
        onClickListeners(btn_playSolo_option3);
        onClickListeners(btn_playSolo_option4);

        Log.d(StringConfig.LOG_TAG, "getDategetDate : " + dateTime.getDateTime());

    }

    //retrieves database values and displays the values to the widgets(textview, buttons etc..)
    private void displayQA(int questionNumber) {

        int i = questionNumber;
        while (captainSQLiteAdapter.selectIsAnswered(i).equalsIgnoreCase("false")) {

            Log.d(StringConfig.LOG_TAG, "iValue : " + i);

            String question = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectQuestion(i));
            String option1 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption1(i));
            String option2 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption2(i));
            String option3 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption3(i));
            String option4 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption4(i));

            String difficulty = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectDifficulty(i));
            String category = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectCategory(i));
            String isAnswered = captainSQLiteAdapter.selectIsAnswered(i);

            new ShuffleChoices(this).getChoicesFromDB(i, btn_playSolo_option1, btn_playSolo_option2, btn_playSolo_option3, btn_playSolo_option4);

            Log.d(StringConfig.LOG_TAG, "question decrypt : " + captainSQLiteAdapter.selectQuestion(i));
            Log.d(StringConfig.LOG_TAG, "question : " + question);
            Log.d(StringConfig.LOG_TAG, "option1 : " + option1);
            Log.d(StringConfig.LOG_TAG, "option2 : " + option2);
            Log.d(StringConfig.LOG_TAG, "option3 : " + option3);
            Log.d(StringConfig.LOG_TAG, "option4 : " + option4);
            Log.d(StringConfig.LOG_TAG, "difficulty : " + difficulty);
            Log.d(StringConfig.LOG_TAG, "category : " + category);
            Log.d(StringConfig.LOG_TAG, "isAnswered : " + isAnswered);

            tv_playSolo_question.setText(question);

            displayQuestionNumber(captainSQLiteAdapter.selectQuestion(i));

            break;
        }

    }

    private void displayQuestionNumber(String question) {

        String questionNumber = captainSQLiteAdapter.selectId(question);
        String category = captainSQLiteAdapter.selectCategory(Integer.parseInt(questionNumber));

        tv_playSolo_question_number.setText(questionNumber);
        tv_playSolo_category.setText(base64DecoderEncoder.base64Decoder(category));

        String decodedCategory = base64DecoderEncoder.base64Decoder(category);

        switch(decodedCategory){

            case "General Knowledge":
                tb.setBackgroundColor(getResources().getColor(R.color.red_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.red_500));
                break;
            case "Entertainment: Books":
                tb.setBackgroundColor(getResources().getColor(R.color.pink_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.pink_500));
                break;
            case "Entertainment: Film":
                tb.setBackgroundColor(getResources().getColor(R.color.purple_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.purple_500));
                break;
            case "Entertainment: Music":
                tb.setBackgroundColor(getResources().getColor(R.color.deep_purple_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.deeppurple_500));
                break;
            case "Entertainment: Musicals & Theatres":
                tb.setBackgroundColor(getResources().getColor(R.color.indigo_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.deeppurple_500));
                break;
            case "Entertainment: Television":
                tb.setBackgroundColor(getResources().getColor(R.color.blue_800));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.blue_600));
                break;
            case "Entertainment: Video Games":
                tb.setBackgroundColor(getResources().getColor(R.color.light_blue_900));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.light_blue_700));
                break;
            case "Entertainment: Board Games":
                tb.setBackgroundColor(getResources().getColor(R.color.cyan_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.cyan_500));
                break;
            case "Science & Nature":
                tb.setBackgroundColor(getResources().getColor(R.color.teal_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.teal_500));
                break;
            case "Science: Computers":
                tb.setBackgroundColor(getResources().getColor(R.color.green_800));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.green_600));
                break;
            case "Science: Mathematics":
                tb.setBackgroundColor(getResources().getColor(R.color.light_green_900));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.light_green_800));
                break;
            case "Mythology":
                tb.setBackgroundColor(getResources().getColor(R.color.lime_900));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.lime_700));
                break;
            case "Sports":
                tb.setBackgroundColor(getResources().getColor(R.color.orange_900));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.orange_700));
                break;
            case "Geography":
                tb.setBackgroundColor(getResources().getColor(R.color.deep_orange_800));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.deep_orange_600));
                break;
            case "History":
                tb.setBackgroundColor(getResources().getColor(R.color.brown_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.brown_500));
                break;
            case "Politics":
                tb.setBackgroundColor(getResources().getColor(R.color.grey_800));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.grey_600));
                break;
            case "Art":
                tb.setBackgroundColor(getResources().getColor(R.color.blue_grey_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.blue_grey_500));
                break;
            case "Celebrities":
                tb.setBackgroundColor(getResources().getColor(R.color.pink_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.pink_500));
                break;
            case "Animals":
                tb.setBackgroundColor(getResources().getColor(R.color.cyan_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.cyan_500));
                break;
            case "Vehicles":
                tb.setBackgroundColor(getResources().getColor(R.color.black));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.grey_800));
                break;
            case "Entertainment: Comics":
                tb.setBackgroundColor(getResources().getColor(R.color.blue_grey_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.blue_grey_500));
                break;
            case "Science: Gadgets":
                tb.setBackgroundColor(getResources().getColor(R.color.lime_900));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.lime_700));
                break;
            case "Entertainment: Japanese Anime & Manga":
                tb.setBackgroundColor(getResources().getColor(R.color.green_800));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.green_600));
                break;
            case "Entertainment: Cartoon & Animations":
                tb.setBackgroundColor(getResources().getColor(R.color.teal_700));
                cv_playSolo_question.setCardBackgroundColor(getResources().getColor(R.color.teal_500));
                break;
        }

        //pb_playSolo.setProgress(Integer.parseInt(questionNumber));

    }

    //initialize options
    private void onClickListeners(final Button button) {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //disable button to prevent double clicks
                btn_playSolo_option1.setEnabled(false);
                btn_playSolo_option2.setEnabled(false);
                btn_playSolo_option3.setEnabled(false);
                btn_playSolo_option4.setEnabled(false);

                String question = tv_playSolo_question.getText().toString();
                String encodedQuestion = base64DecoderEncoder.base64Encoder(question);

                String correctAnswer = captainSQLiteAdapter.selectOption1(Integer.parseInt(tv_playSolo_question_number.getText().toString()));
                String decodedAnswer = base64DecoderEncoder.base64Decoder(correctAnswer);
                String selectedAnswer = button.getText().toString();

                Log.d(StringConfig.LOG_TAG, "encodedQuestion : " + encodedQuestion);
                Log.d(StringConfig.LOG_TAG, "decodedAnswer : " + decodedAnswer);
                Log.d(StringConfig.LOG_TAG, "selectedAnswer : " + selectedAnswer);

                if (Integer.parseInt(tv_playSolo_question_number.getText().toString()) >= 100) {

                    tv_playSolo_timeLeft.setText(String.valueOf(0));

                    countdownTimerCancel();

                    submitScore();

                } else {

                    if (decodedAnswer.equals(selectedAnswer)) {

                        //Toast.makeText(PlaySoloActivity.this, "Answer is correct : " + decodedAnswer + " : " + selectedAnswer, Toast.LENGTH_SHORT).show();

                        int getCorrect = pointingManager.getCorrect();
                        pointingManager.setCorrect(getCorrect + 1);

                        calculatePoints();

                    } else {
                        //Toast.makeText(PlaySoloActivity.this, "Answer is wrong : " + decodedAnswer + " : " + selectedAnswer, Toast.LENGTH_SHORT).show();

                        animateButton.wrongAnswer(button);

                    }

                    animateButton.correctAnswer(decodedAnswer, btn_playSolo_option1, btn_playSolo_option2, btn_playSolo_option3, btn_playSolo_option4);
                    //animateCorrectAnswerButton(decodedAnswer);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            /*countdownTimerCancel();
                            countdownTimerStart();*/
                            initNextQuestion();
                        }
                    }, 1200);


                }

            }
        });
    }

    private void initNextQuestion() {


        btn_playSolo_option1.setEnabled(true);
        btn_playSolo_option2.setEnabled(true);
        btn_playSolo_option3.setEnabled(true);
        btn_playSolo_option4.setEnabled(true);

        animateButton.defaultButtonsSurvival(btn_playSolo_option1, btn_playSolo_option2, btn_playSolo_option3, btn_playSolo_option4);

        if (Integer.parseInt(tv_playSolo_question_number.getText().toString()) >= 100) {

            submitScore();

        } else {

            int answeredQuestionNumber = answeredQuestionsManager.getAnsweredQuestions() + 1;

            answeredQuestionsManager.setAnsweredQuestions(answeredQuestionNumber);

            //updates isAnswered column in db, writes true if the question is answered
            captainSQLiteAdapter.updateIsAnswered(answeredQuestionsManager.getAnsweredQuestions() - 1);

            displayQA(answeredQuestionNumber);

            /*countdownTimerCancel();
            countdownTimerStart();*/

        }

    }

    private void countdownTimerStart() {

        Log.d(StringConfig.LOG_TAG, "timer : timerCalled");

        countDownTimer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                int timeLeft = (int) (millisUntilFinished / 1000);
                tv_playSolo_timeLeft.setText(String.valueOf(timeLeft));
                Log.d(StringConfig.LOG_TAG, "survivalActivity:timer : onTickCalled");
            }

            @Override
            public void onFinish() {

                Log.d(StringConfig.LOG_TAG, "timer : onFinishCalled");

                /*String correctAnswer = captainSQLiteAdapter.selectOption1(Integer.parseInt(tv_playSolo_question_number.getText().toString()));
                String decodedAnswer = base64DecoderEncoder.base64Decoder(correctAnswer);

                animateButton.correctAnswer(decodedAnswer, btn_playSolo_option1, btn_playSolo_option2, btn_playSolo_option3, btn_playSolo_option4);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initNextQuestion();
                    }
                }, 1200);*/

                submitScore();

            }
        }.start();

    }

    private void countdownTimerCancel() {
        if (countDownTimer != null) {
            Log.d(StringConfig.LOG_TAG, "survivalActivity:countDownTimerCancel called");
            countDownTimer.cancel();
        }
    }

    private void calculatePoints() {

        int points = Integer.parseInt(tv_playSolo_timeLeft.getText().toString());

        /*int getCurrentPointsOnSpecificCategory = pointingManager.getPointsOnSpecificCategory(tv_playSolo_category.getText().toString());

        pointingManager.setPointsOnSpecificCategory(tv_playSolo_category.getText().toString(), getCurrentPointsOnSpecificCategory + points);*/


        if (String.valueOf(pointingManager.getPoints()) != null) {

            int getCurrentPoints = pointingManager.getPoints();
            pointingManager.setPoints(points + getCurrentPoints);

        } else {

            pointingManager.setPoints(points);

        }


    }

    private void submitScore() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Calculating score...");
        progressDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();

                String totalQuestions = tv_playSolo_question_number.getText().toString();
                pointingManager.setTotal(totalQuestions);

                int points = pointingManager.getPoints();
                Bundle bundle = new Bundle();
                bundle.putInt("points", points);

                Intent intent = new Intent(SurvivalActivity.this, GameResultActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

                finish();

            }
        }, 1500);


    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Surrender ?")
                .setMessage("Are you sure captain ? Your crew needs you !")
                .setPositiveButton("Aye, aye !", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        countdownTimerCancel();

                        captainSQLiteAdapter.deleteContents();
                        startActivity(new Intent(SurvivalActivity.this, HomeActivity.class));
                        finish();

                    }
                })
                .setNegativeButton("Noooooo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //countdownTimerCancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(StringConfig.LOG_TAG, "survivalActivity:onDestroyCalled");
        countdownTimerCancel();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


  /*  public void cancelHandler(){
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

            }
        };


        handler.postDelayed(runnable, 2);

    }*/
}
