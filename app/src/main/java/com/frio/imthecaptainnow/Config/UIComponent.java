package com.frio.imthecaptainnow.Config;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by steven on 12/16/17.
 */

public class UIComponent {
    private ProgressDialog progressDialog;
    Context context;

    public UIComponent(Context context){
        this.context = context;
    }

    public ProgressDialog loginDialog(){

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Logging in...");

        return progressDialog;
    }

    public ProgressDialog signUpDialog(){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Signing up...");

        return progressDialog;
    }

    public ProgressDialog signoutDialog(){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Logging out...");

        return progressDialog;
    }

    public ProgressDialog pleaseWaitDialog(){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Please wait...");

        return progressDialog;
    }

    public ProgressDialog initializeGameSessionDialog(){
        ProgressDialog progressDialog;
        progressDialog = ProgressDialog.show(context, "", "Initializing game session.");
        return progressDialog;
    }

    /*public ProgressDialog initializeGameSessionDialog(){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Initializing game session.");
        progressDialog.show();
        return progressDialog;
    }*/

    public ProgressDialog fetchGameCategoryDialog(){
        ProgressDialog progressDialog;
        progressDialog = ProgressDialog.show(context, "", "Retrieving data from server.");
        return progressDialog;
    }

}
