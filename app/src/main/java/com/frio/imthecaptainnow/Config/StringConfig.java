package com.frio.imthecaptainnow.Config;

/**
 * Created by steven on 12/14/17.
 */

public class StringConfig {

    public static final String LOG_TAG = "TriviaGame";

    public static final String SHAREDPREF_NAME_SESSION_TOKEN = "SessionToken";
    public static final String SHAREDPREF_SESSION_TOKEN_KEY = "SessionToken";

    public static final String SHAREDPREF_NAME_CATEGORY_MANAGER = "GameCategory";
    public static final String SHAREDPEF_CATEGORY_KEY_ID = "category_id";
    public static final String SHAREDPREF_CATEGORY_KEY_NAME = "category_name";

    public static final String SHAREDPREF_NAME_ANSWERED_QUESTIONS = "AnsweredQuestions";
    public static final String SHAREDPREF_QUESTION_NO_KEY = "question_no";

    public static final String SHAREDPREF_NAME_POINTING = "Pointing";
    public static final String SHAREDPREF_POINTING_CATEGORY_KEY_ID = "points";
    public static final String SHAREDPREF_POINTING_KEY_ANSWERED = "answered";
    public static final String SHAREDPREF_POINTING_KEY_TOTAL = "total";


    public static final String SHAREDPREF_CAMPAIGN_PROGRESS = "CampaignProgress";
    public static final String SHAREDPREF_CAMPAIGN_PROGRESS_KEY = "progress";
    public static final String SHAREDPREF_CAMPAIGN_LIVES_KEY = "lives";
    public static final String SHAREDPREF_CAMPAIGN_ANSWERED_QUESTION_KEY = "answered_question";

    public static final String SHAREDPREF_SAVE_PROGRESS = "SaveProgress";
    public static final String SHAREDPREF_SAVE_KEY_LIVES = "lives_remaining";
    public static final String SHAREDPREF_SAVE_KEY_PROGRESS = "progress";


    //opentdb routes
    public static final String OPENTDB_URL_RETRIEVE_SESSION_TOKEN = "https://opentdb.com/api_token.php?command=request";
    public static final String OPENTDB_URL_RETRIEVE_SESSION_TOKEN_RESPONSE_CODE = "response_code";
    public static final String OPENTDB_URL_RETRIEVE_SESSION_TOKEN_RESPONSE_MESSAGE = "response_message";
    public static final String OPENTDB_URL_RETRIEVE_SESSION_TOKEN_RESPONSE_TOKEN = "token";

    //opentdb response codes
    public static final String OPENTDB_RESPONSE_CODE_0 = "Success";  //Returned results successfully.
    public static final String OPENTDB_RESPONSE_CODE_1 = "No Results"; // Could not return results. The API doesn't have enough questions for your query. (Ex. Asking for 50 Questions in a Category that only has 20.)
    public static final String OPENTDB_RESPONSE_CODE_2 = "Invalid Parameter"; //Contains an invalid parameter. Arguments passed in aren't valid. (Ex. Amount = Five)
    public static final String OPENTDB_RESPONSE_CODE_3 = "Token Not Found"; //Session token does not exist
    public static final String OPENTDB_RESPONSE_CODE_4 = "Token Empty"; // Session Token has returned all possible questions for the specified query. Resetting the Token is necessary.

    public static final String OPENTDB_URL_RETRIEVE_CATEGORY = "https://opentdb.com/api_category.php?encode=base64";
    public static final String OPENTDB_URL_RETRIEVE_CATEGORY_RESPONSE_CATEGORIES = "trivia_categories";
    public static final String OPENTDB_URL_RETRIEVE_CATEGORY_RESPONSE_ID = "id";
    public static final String OPENTDB_URL_RETRIEVE_CATEGORY_RESPONSE_NAME = "name";

    public static final String OPENTDB_URL_TRIVIA_FEED = "https://opentdb.com/api.php?amount=10&type=multiple&encode=base64";

    public static final String OPENTDB_URL_TRIVIA_FEED_FILTER_CATEGORY(int category) {

        return "https://opentdb.com/api.php?amount=10&category=" + category + "&type=multiple&encode=base64";

    }

    public static final String OPENTDB_URL_CAPTAIN_QUESTIONS = "https://opentdb.com/api.php?amount=20&difficulty=easy&type=multiple";

    //PLAY RANDOMLY
    public static final String OPENTDB_URL_RANDOM = "https://opentdb.com/api.php?amount=80&difficulty=easy&type=multiple&encode=base64";

    public static String OPENTDB_URL_NONRANDOM(int categoryId, String difficulty) {

        String nonRandomURL = "https://opentdb.com/api.php?amount=10&category=" + categoryId + "&difficulty=" + difficulty + "&type=multiple&encode=base64";

        return nonRandomURL;

    }

}
