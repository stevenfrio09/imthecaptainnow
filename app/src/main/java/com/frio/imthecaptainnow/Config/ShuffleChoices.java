package com.frio.imthecaptainnow.Config;

import android.content.Context;
import android.util.Log;
import android.widget.Button;

import com.frio.imthecaptainnow.SQLite.CaptainSQLiteAdapter;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by steven on 12/26/17.
 */

public class ShuffleChoices {

    Context context;
    Base64DecoderEncoder base64DecoderEncoder;
    CaptainSQLiteAdapter captainSQLiteAdapter;
    CaptainSQLiteAdapter.CaptainSQLite captainSQLite;
    Button button1, button2, button3, button4;


    public ShuffleChoices(Context context) {
        this.context = context;
        captainSQLiteAdapter = new CaptainSQLiteAdapter(context);
        base64DecoderEncoder = new Base64DecoderEncoder();

    }

    public void getChoicesFromDB(int id, Button button1, Button button2, Button button3, Button button4) {

        String option1 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption1(id));
        String option2 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption2(id));
        String option3 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption3(id));
        String option4 = base64DecoderEncoder.base64Decoder(captainSQLiteAdapter.selectOption4(id));

        String[] array = {option1, option2, option3, option4};
        //Log.d(StringConfig.LOG_TAG, "BEFORE : " + Arrays.toString(array));

        //FisherYatesShuffle(array, 0);

        FisherYatesShuffle(array);

        String array0 = array[0];
        String array1 = array[1];
        String array2 = array[2];
        String array3 = array[3];

        /*Log.d(StringConfig.LOG_TAG, "AFTER : ARRAY : " + Arrays.toString(array));

        Log.d(StringConfig.LOG_TAG, "AFTER : OPTION1 : " + array0);
        Log.d(StringConfig.LOG_TAG, "AFTER : OPTION2 : " + array1);
        Log.d(StringConfig.LOG_TAG, "AFTER : OPTION3 : " + array2);
        Log.d(StringConfig.LOG_TAG, "AFTER : OPTION4 : " + array3);*/

        button1.setText(array0);
        button2.setText(array1);
        button3.setText(array2);
        button4.setText(array3);

    }

    public void FisherYatesShuffle(String[] array) {
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            int index = random.nextInt(i);

            String tmp = array[index];
            array[index] = array[i];
            array[i] = tmp;
        }
    }

}
