package com.frio.imthecaptainnow.Config;

import android.util.Base64;

import java.nio.charset.StandardCharsets;

/**
 * Created by steven on 12/21/17.
 */

public class Base64DecoderEncoder {

    public String base64Decoder(String base64Value){

        byte[] encodedValue = Base64.decode(base64Value, Base64.DEFAULT);
        String decodedValue = new String(encodedValue, StandardCharsets.UTF_8);

        return decodedValue;
    }

    public String base64Encoder(String stringValue){
        // Sending side
        byte[] data = stringValue.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);

        return base64;

    }


}
