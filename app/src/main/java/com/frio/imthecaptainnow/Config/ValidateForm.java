package com.frio.imthecaptainnow.Config;

import android.support.design.widget.TextInputLayout;
import android.util.Log;

import com.frio.imthecaptainnow.Config.StringConfig;

/**
 * Created by steven on 12/15/17.
 */


public class ValidateForm {

    //checks whether a field is valid or not, returns true if valid
    public boolean validateLogin(String email, String password, TextInputLayout til_logEmail, TextInputLayout til_logPassword) {

        boolean valid = true;

        if (isEmptyLoginField(email, til_logEmail)) {
            valid = false;
        }
        if (isEmptyLoginField(password, til_logPassword)) {
            valid = false;
        }

        return valid;

    }

    //checks whether a field is empty or not, returns true if empty
    public boolean isEmptyLoginField(String value, TextInputLayout textInputLayout) {

        boolean empty = false;

        if (value.isEmpty()) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError("This field is required.");
            empty = true;
        } else {
            textInputLayout.setErrorEnabled(false);
        }

        return empty;
    }

    public boolean validateSignup(String username, String email, String password, String passwordConfirmation,
                                  TextInputLayout til_username, TextInputLayout til_email, TextInputLayout til_password, TextInputLayout til_passwordConfirmation) {
        boolean valid = true;

        if (isEmptySignupField(username, til_username)) {
            valid = false;
        }

        if (isEmptySignupField(email, til_email)) {
            valid = false;
        }

        if (isEmptySignupField(password, til_password)) {
            valid = false;
        }

        if (isEmptySignupField(passwordConfirmation, til_passwordConfirmation)) {
            valid = false;
        }

        if(!email.contains(".com") && !email.contains("@")){
            valid = false;
            Log.d(StringConfig.LOG_TAG, "invalid email : " + email);
        }

        if (!passwordConfirmation.equals(password)) {

            valid = false;

            til_password.setErrorEnabled(true);
            til_password.setError("Password does not match");

            til_passwordConfirmation.setErrorEnabled(true);
            til_passwordConfirmation.setError("Password does not match");

        }

        if (password.length() < 6) {

            valid = false;

            til_password.setErrorEnabled(true);
            til_password.setError("Password should be at least 6 characters.");
        }

        if (passwordConfirmation.length() < 6) {

            valid = false;

            til_passwordConfirmation.setErrorEnabled(true);
            til_password.setError("Password should be at least 6 characters.");

        }

        return valid;
    }

    public boolean isEmptySignupField(String value, TextInputLayout textInputLayout) {
        boolean empty = false;

        if (value.isEmpty()) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError("This field is required");
            empty = true;
        } else {
            textInputLayout.setErrorEnabled(false);
        }
        return empty;
    }
}
