package com.frio.imthecaptainnow.Config;

import android.support.design.widget.TextInputLayout;

/**
 * Created by steven on 12/6/17.
 */

public class ValidateFields {
    public boolean validateTrivia(String question, String answer_correct, String answer_incorrect01, String answer_incorrect02, String answer_incorrect03,
                                  TextInputLayout til_question, TextInputLayout til_answer_correct, TextInputLayout til_answer_incorrect01, TextInputLayout til_answer_incorrect02, TextInputLayout til_answer_incorrect03) {
        boolean valid = true;
        if (question.isEmpty()) {
            displayTextInputLayoutErrors(til_question);
            valid = false;
        }
        if (answer_correct.isEmpty()) {
            displayTextInputLayoutErrors(til_answer_correct);
            valid = false;
        }
        if (answer_incorrect01.isEmpty()) {
            displayTextInputLayoutErrors(til_answer_incorrect01);
            valid = false;
        }

        if (answer_incorrect02.isEmpty()) {
            displayTextInputLayoutErrors(til_answer_incorrect02);
            valid = false;
        }
        if (answer_incorrect03.isEmpty()) {
            displayTextInputLayoutErrors(til_answer_incorrect03);
            valid = false;
        }

        return valid;
    }

    public void displayTextInputLayoutErrors(TextInputLayout textInputLayout) {
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError("This field is required!");
    }
}
