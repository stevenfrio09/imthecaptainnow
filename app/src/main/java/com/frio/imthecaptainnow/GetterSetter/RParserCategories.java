package com.frio.imthecaptainnow.GetterSetter;

/**
 * Created by John Steven Frio on 2/1/2018.
 */

public class RParserCategories {
    private int id;
    private String category;


    public RParserCategories(int id, String category) {
        this.id = id;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
