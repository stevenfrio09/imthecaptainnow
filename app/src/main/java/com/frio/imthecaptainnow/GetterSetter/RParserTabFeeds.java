package com.frio.imthecaptainnow.GetterSetter;

/**
 * Created by steven on 12/14/17.
 */

public class RParserTabFeeds {
    private String category;
    private String difficulty;
    private String question;
    private String answer_correct;

    public RParserTabFeeds(String category, String difficulty, String question, String answer_correct) {
        this.category = category;
        this.difficulty = difficulty;
        this.question = question;
        this.answer_correct = answer_correct;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer_correct() {
        return answer_correct;
    }

    public void setAnswer_correct(String answer_correct) {
        this.answer_correct = answer_correct;
    }

}
