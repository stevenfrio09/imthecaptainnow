package com.frio.imthecaptainnow.GetterSetter;

/**
 * Created by steven on 1/16/18.
 */

public class RParserHistory {

    private String date;
    private int correct;
    private int total;

    public RParserHistory(String date, int correct, int total){
        this.date = date;
        this.correct = correct;
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
