package com.frio.imthecaptainnow;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.frio.imthecaptainnow.Config.Base64DecoderEncoder;
import com.frio.imthecaptainnow.Config.StringConfig;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContributeQuestionsMainActivity extends AppCompatActivity {
    TextInputEditText tet_question, tet_answer, tet_option1, tet_option2, tet_option3;
    TextInputLayout til_question, til_answer, til_option1, til_option2, til_option3;

    Button  btn_clear;
    FloatingActionButton fab_submit;

    String intentValue;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    Base64DecoderEncoder base64DecoderEncoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_contribute_questions_main);

        tet_question = findViewById(R.id.tet_question);
        tet_answer = findViewById(R.id.tet_answer);
        tet_option1 = findViewById(R.id.tet_option1);
        tet_option2 = findViewById(R.id.tet_option2);
        tet_option3 = findViewById(R.id.tet_option3);

        til_question = findViewById(R.id.til_question);
        til_answer = findViewById(R.id.til_answer);
        til_option1 = findViewById(R.id.til_option1);
        til_option2 = findViewById(R.id.til_option2);
        til_option3 = findViewById(R.id.til_option3);

        fab_submit = findViewById(R.id.fab_submit);
        btn_clear = findViewById(R.id.btn_clear);

        Bundle bundle = getIntent().getExtras();


        intentValue = bundle.getString("intentValue");
        Log.d("TAG", "intentValue : " + intentValue);

        setTitle(intentValue);


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(intentValue);

        base64DecoderEncoder = new Base64DecoderEncoder();

        onClickListener();

        onTouchListeners(til_question, tet_question);
        onTouchListeners(til_answer, tet_answer);
        onTouchListeners(til_option1, tet_option1);
        onTouchListeners(til_option2, tet_option2);
        onTouchListeners(til_option3, tet_option3);

    }

    private void onClickListener() {

        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearTextFields();

            }
        });

        fab_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String question = tet_question.getText().toString();
                String answer = tet_answer.getText().toString();
                String option1 = tet_option1.getText().toString();
                String option2 = tet_option2.getText().toString();
                String option3 = tet_option3.getText().toString();

                if (isValidFields(question, answer, option1, option2, option3)) {
                    Log.d(StringConfig.LOG_TAG, "isValidFields : " + isValidFields(question, answer, option1, option2, option3));

                    final AlertDialog.Builder builder = new AlertDialog.Builder(ContributeQuestionsMainActivity.this);
                    //builder.setTitle("Confirm");
                    builder.setMessage(
                            "Category : " + intentValue + "\n" +
                                    "Question : " + question + "\n" +
                                    "Answer : " + answer + "\n" +
                                    "Incorrect Answer 01: " + option1 + "\n" +
                                    "Incorrect Answer 02: " + option2 + "\n" +
                                    "Incorrect Answer 03: " + option3 + "\n")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    uploadTrivia();

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                }
                            })
                            .show();
                }
            }
        });


    }

    private void clearTextFields() {
        tet_question.setText("");
        tet_answer.setText("");
        tet_option1.setText("");
        tet_option2.setText("");
        tet_option3.setText("");
    }

    private void onTouchListeners(final TextInputLayout textInputLayout, final TextInputEditText textInputEditText) {
        textInputEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textInputLayout.setErrorEnabled(false);
                return false;
            }
        });
    }

    //VALIDATE  FIELDS
    private boolean isValidFields(String question, String answer, String option1, String option2, String option3) {
        boolean valid = false;

        if (!(question.isEmpty() || answer.isEmpty() || option1.isEmpty() || option2.isEmpty() || option3.isEmpty())) {
            valid = true;
        }

        if (question.isEmpty()) {
            til_question.isErrorEnabled();
            til_question.setError("This field is required.");
        }

        if (answer.isEmpty()) {
            til_answer.isErrorEnabled();
            til_answer.setError("This field is required.");
        }

        if (option1.isEmpty()) {
            til_option1.isErrorEnabled();
            til_option1.setError("This field is required.");
        }

        if (option2.isEmpty()) {

            til_option2.isErrorEnabled();
            til_option2.setError("This field is required.");
        }

        if (option3.isEmpty()) {

            til_option3.isErrorEnabled();
            til_option3.setError("This field is required.");
        }
        return valid;
    }

    private void uploadTrivia() {

        final ProgressDialog progressDialog = new ProgressDialog(ContributeQuestionsMainActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long childrenCount = dataSnapshot.getChildrenCount();

                Log.d(StringConfig.LOG_TAG, "firebaseDatabaseChildrenCount : " + childrenCount);

                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("category").setValue(intentValue);
                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("correct_answer").setValue(tet_answer.getText().toString());
                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("difficulty").setValue("easy");

                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("incorrect_answers").child("0").setValue(tet_option1.getText().toString());
                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("incorrect_answers").child("1").setValue(tet_option2.getText().toString());
                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("incorrect_answers").child("2").setValue(tet_option3.getText().toString());

                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("question").setValue(tet_question.getText().toString());
                firebaseDatabase.getReference(intentValue).child(String.valueOf(childrenCount)).child("type").setValue("multiple");


                progressDialog.dismiss();

                clearTextFields();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(StringConfig.LOG_TAG, "databaseError : " + databaseError);

                progressDialog.dismiss();

            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
