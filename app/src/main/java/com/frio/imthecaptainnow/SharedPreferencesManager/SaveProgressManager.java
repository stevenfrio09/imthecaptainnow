package com.frio.imthecaptainnow.SharedPreferencesManager;

import android.content.Context;
import android.content.SharedPreferences;

import com.frio.imthecaptainnow.Config.StringConfig;

/**
 * Created by steven on 1/24/18.
 */

public class SaveProgressManager {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public SaveProgressManager(Context context) {
        sharedPreferences = context.getSharedPreferences(StringConfig.SHAREDPREF_SAVE_PROGRESS, 0);
        editor = sharedPreferences.edit();

    }

    public void setRemainingLives(int remainingLives) {
        editor.putInt(StringConfig.SHAREDPREF_SAVE_KEY_LIVES, remainingLives);
        editor.commit();
    }

    public int getRemainingLives() {
        return sharedPreferences.getInt(StringConfig.SHAREDPREF_SAVE_KEY_LIVES, 0);
    }


    public void setProgress(String progress) {
        editor.putString(StringConfig.SHAREDPREF_SAVE_PROGRESS, progress);
        editor.commit();
    }

    public String getProgress() {
        return sharedPreferences.getString(StringConfig.SHAREDPREF_SAVE_PROGRESS, null);
    }


    public void clearSharedPreferences() {

        editor.clear();
        editor.commit();

    }

    public boolean isSharedPrefExisting() {

        boolean isExisting = false;

        if (sharedPreferences.contains(StringConfig.SHAREDPREF_SAVE_KEY_LIVES) || sharedPreferences.contains(StringConfig.SHAREDPREF_SAVE_KEY_PROGRESS)) {

            isExisting = true;
        } 

        return isExisting;

    }
}
