package com.frio.imthecaptainnow.SharedPreferencesManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.frio.imthecaptainnow.Config.StringConfig;

/**
 * Created by steven on 1/17/18.
 */

public class CampaignProgressManager {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public CampaignProgressManager(Context context){
        sharedPreferences = context.getSharedPreferences(StringConfig.SHAREDPREF_CAMPAIGN_PROGRESS, 0);
        editor = sharedPreferences.edit();
    }

    public void setCampaignProgress(String progress){
        editor.putString(StringConfig.SHAREDPREF_CAMPAIGN_PROGRESS_KEY, progress);
        editor.commit();
    }

    public String getCampaignProgress(){
        return sharedPreferences.getString(StringConfig.SHAREDPREF_CAMPAIGN_PROGRESS_KEY, null);
    }

    public void setCampaignLives(int lives){
        editor.putInt(StringConfig.SHAREDPREF_CAMPAIGN_LIVES_KEY, lives);
        editor.commit();
    }

    public int getCampaignLives(){
        return sharedPreferences.getInt(StringConfig.SHAREDPREF_CAMPAIGN_LIVES_KEY, 0);
    }

    public void setAnsweredQuestions(int value){
        editor.putInt(StringConfig.SHAREDPREF_CAMPAIGN_ANSWERED_QUESTION_KEY, value);
        editor.commit();
    }

    public int getAnsweredQuestions(){
        return sharedPreferences.getInt(StringConfig.SHAREDPREF_CAMPAIGN_ANSWERED_QUESTION_KEY, 0);
    }


    public void clearSharedPreferences(){
        editor.clear();
        editor.commit();
    }

    public boolean isSharedPrefExisting(){
        boolean isExisting = false;

        if(sharedPreferences.contains(StringConfig.SHAREDPREF_CAMPAIGN_LIVES_KEY)){
            isExisting = true;
        }


        return isExisting;

    }

}

