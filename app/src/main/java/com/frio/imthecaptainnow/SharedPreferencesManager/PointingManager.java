package com.frio.imthecaptainnow.SharedPreferencesManager;

import android.content.Context;
import android.content.SharedPreferences;

import com.frio.imthecaptainnow.Config.StringConfig;

/**
 * Created by steven on 1/4/18.
 */

public class PointingManager {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public PointingManager(Context context){
        sharedPreferences = context.getSharedPreferences(StringConfig.SHAREDPREF_NAME_POINTING, 0);
        editor = sharedPreferences.edit();
    }

    public void setPoints(int points){

        editor.putInt(StringConfig.SHAREDPREF_POINTING_CATEGORY_KEY_ID, points);
        editor.apply();
    }

    public int getPoints(){
        return sharedPreferences.getInt(StringConfig.SHAREDPREF_POINTING_CATEGORY_KEY_ID, 0);
    }

    public void setPointsOnSpecificCategory(String category, int points){
        editor.putInt(category, points);
        editor.apply();
    }

    public int getPointsOnSpecificCategory(String category){
        return sharedPreferences.getInt(category, 0);
    }

    public void clearSharedPrefPointing(){
        editor.clear();
        editor.commit();
    }

    public void setCorrect(int answered){
        editor.putInt(StringConfig.SHAREDPREF_POINTING_KEY_ANSWERED, answered);
        editor.commit();
    }

    public int getCorrect(){
        return sharedPreferences.getInt(StringConfig.SHAREDPREF_POINTING_KEY_ANSWERED, 0);
    }

    public void setTotal(String total){
        editor.putString(StringConfig.SHAREDPREF_POINTING_KEY_TOTAL, total);
        editor.commit();
    }

    public String getTotal(){
        return sharedPreferences.getString(StringConfig.SHAREDPREF_POINTING_KEY_TOTAL, null);
    }
}
