package com.frio.imthecaptainnow.SharedPreferencesManager;

import android.content.Context;
import android.content.SharedPreferences;

import com.frio.imthecaptainnow.Config.StringConfig;

/**
 * Created by steven on 12/24/17.
 */

public class AnsweredQuestionsManager {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public AnsweredQuestionsManager(Context context){
        sharedPreferences = context.getSharedPreferences(StringConfig.SHAREDPREF_NAME_ANSWERED_QUESTIONS, 0);
        editor = sharedPreferences.edit();
    }

    public void setAnsweredQuestions(int value){
        editor.putInt(StringConfig.SHAREDPREF_QUESTION_NO_KEY, value);
        editor.apply();
    }

    public int getAnsweredQuestions(){
        return sharedPreferences.getInt(StringConfig.SHAREDPREF_QUESTION_NO_KEY, 0);

    }

    public void deleteAnsweredQuestions(){
        editor.clear();
        editor.apply();
    }

}
