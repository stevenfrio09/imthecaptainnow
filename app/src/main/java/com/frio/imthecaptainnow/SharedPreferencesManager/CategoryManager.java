package com.frio.imthecaptainnow.SharedPreferencesManager;

import android.content.Context;
import android.content.SharedPreferences;

import com.frio.imthecaptainnow.Config.StringConfig;

/**
 * Created by steven on 12/21/17.
 */

public class CategoryManager {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public CategoryManager(Context context) {
        sharedPreferences = context.getSharedPreferences(StringConfig.SHAREDPREF_NAME_CATEGORY_MANAGER, 0);
        editor = sharedPreferences.edit();
    }

    public void setCategoryName(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getCategoryValue(String key) {
        return sharedPreferences.getString(key, null);
    }

    public void clearSharedPref_category() {
        editor.clear();
        editor.apply();
    }

    public boolean isEmptyCategory() {
        boolean isEmpty;

        if (sharedPreferences == null) {
            isEmpty = true;
        } else {
            isEmpty = false;
        }

        return isEmpty;
    }

}

