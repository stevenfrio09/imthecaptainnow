package com.frio.imthecaptainnow;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.frio.imthecaptainnow.Config.Base64DecoderEncoder;
import com.frio.imthecaptainnow.Config.StringConfig;
import com.frio.imthecaptainnow.SQLite.CaptainSQLiteAdapter;
import com.frio.imthecaptainnow.SharedPreferencesManager.AnsweredQuestionsManager;
import com.frio.imthecaptainnow.SharedPreferencesManager.CampaignProgressManager;
import com.frio.imthecaptainnow.SharedPreferencesManager.SaveProgressManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CampaignProgressActivity extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    TextView tv_campaignCategory;
    CircleImageView civ_category01, civ_category02, civ_category03, civ_category04, civ_category05, civ_captain;
    ImageView iv_life01, iv_life02, iv_life03, iv_life04, iv_life05;

    Button btn_campaignContinue;

    //sharedpref
    CampaignProgressManager campaignProgressManager;
    SaveProgressManager saveProgressManager;

    //sqlite
    CaptainSQLiteAdapter captainSQLiteAdapter;

    Base64DecoderEncoder base64DecoderEncoder;

    String strCategory;
    int numQuestions;
    int livesLeft;

    ConstraintLayout cl_timeAttack;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_campaign_progress);

        //Log.d(StringConfig.LOG_TAG, "campaignProgressActivity:onCreate called");

        firebaseDatabase = FirebaseDatabase.getInstance();

        //tv_campaignCategory = findViewById(R.id.tv_campaignCategory);
        iv_life01 = findViewById(R.id.iv_life01);
        iv_life02 = findViewById(R.id.iv_life02);
        iv_life03 = findViewById(R.id.iv_life03);
        iv_life04 = findViewById(R.id.iv_life04);
        iv_life05 = findViewById(R.id.iv_life05);

        civ_category01 = findViewById(R.id.civ_category01);
        civ_category02 = findViewById(R.id.civ_category02);
        civ_category03 = findViewById(R.id.civ_category03);
        civ_category04 = findViewById(R.id.civ_category04);
        civ_category05 = findViewById(R.id.civ_category05);
        civ_captain = findViewById(R.id.civ_captain);

        btn_campaignContinue = findViewById(R.id.btn_campaignContinue);

        campaignProgressManager = new CampaignProgressManager(CampaignProgressActivity.this);

        saveProgressManager = new SaveProgressManager(this);

        campaignProgressManager.setAnsweredQuestions(0);
        //deleteDatabase("CaptainSQLite.db");

        cl_timeAttack = findViewById(R.id.cl_timeAttack);



        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            //this statement is always executed !!!! fix this, the problem is in the
            //saveProgressManager.isSharedPRefexisting

            if (saveProgressManager.isSharedPrefExisting()) {

                Log.d(StringConfig.LOG_TAG, "saveProgressManager sharedPref existing");

                //Log.d(StringConfig.LOG_TAG, "bundle.getString(saveProgress) : " + bundle.getString("saveProgress") );

                if (bundle.getString("saveProgress") != null) {

                    int progress = Integer.parseInt(bundle.getString("saveProgress"));

                    livesLeft = bundle.getInt("saveLives");
                    campaignProgressManager.setCampaignLives(livesLeft);
                    campaignProgressManager.setCampaignProgress(String.valueOf(progress - 1));

                    Log.d(StringConfig.LOG_TAG, "livesLefffttttyyy  : " + livesLeft);

                }
            } else {

                Log.d(StringConfig.LOG_TAG, "campaignProgressManager sharedPref existing");

                livesLeft = bundle.getInt("livesLeft");
                campaignProgressManager.setCampaignLives(livesLeft);

                Log.d(StringConfig.LOG_TAG, "livesLeffftttt  : " + livesLeft);

            }


        } else {

            campaignProgressManager.setCampaignLives(5);

        }

        manageLives(true);

        manageCategoryBadges();

        captainSQLiteAdapter = new CaptainSQLiteAdapter(this);
        captainSQLiteAdapter.deleteContents();

        base64DecoderEncoder = new Base64DecoderEncoder();

        if (campaignProgressManager.getCampaignProgress() == null) {

            campaignProgressManager.setCampaignProgress("1");

            fetchFirebaseData(1, 1500);

        } else {

            int previousCampaignProgress = Integer.parseInt(campaignProgressManager.getCampaignProgress());
            final int currentCampaignProgress = previousCampaignProgress + 1;

            campaignProgressManager.setCampaignProgress(String.valueOf(currentCampaignProgress));

            Log.d(StringConfig.LOG_TAG, "previousCampaignProgress : " + previousCampaignProgress);
            Log.d(StringConfig.LOG_TAG, "currentCampaignProgress : " + currentCampaignProgress);
            //Log.d(StringConfig.LOG_TAG, "currentCampaignProgress : " +e currentCampaignProgress);


            btn_campaignContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(StringConfig.LOG_TAG, "onClickListener called");
                    fetchFirebaseData(currentCampaignProgress, 0);

                }
            });

        }

        onCategoryClick(civ_category01, R.id.civ_category01);
        onCategoryClick(civ_category02, R.id.civ_category02);
        onCategoryClick(civ_category03, R.id.civ_category03);
        onCategoryClick(civ_category04, R.id.civ_category04);
        onCategoryClick(civ_category05, R.id.civ_category05);


    }

    private void onCategoryClick(CircleImageView circleImageView, final int id) {

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (id){
                    case R.id.civ_category01:
                        snackbar = Snackbar.make(cl_timeAttack, "History", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        //Toast.makeText(CampaignProgressActivity.this, "History", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.civ_category02:
                        snackbar = Snackbar.make(cl_timeAttack, "Geography", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        //Toast.makeText(CampaignProgressActivity.this, "Geography", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.civ_category03:
                        snackbar = Snackbar.make(cl_timeAttack, "Math", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        //Toast.makeText(CampaignProgressActivity.this, "Math", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.civ_category04:
                        snackbar = Snackbar.make(cl_timeAttack, "Science", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        //Toast.makeText(CampaignProgressActivity.this, "Science", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.civ_category05:
                        snackbar = Snackbar.make(cl_timeAttack, "PUP", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        //Toast.makeText(CampaignProgressActivity.this, "PUP", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.civ_captain:
                        snackbar = Snackbar.make(cl_timeAttack, "Captain Mode", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                }
            }

        });

    }

    //fetch questions from firebase
    private void fetchFirebaseData(int currentCampaignProgress, final int delay) {

        final ProgressDialog progressDialog = new ProgressDialog(CampaignProgressActivity.this);
        progressDialog.setMessage("Retrieving questions from the database...");
        progressDialog.show();

        switch (String.valueOf(currentCampaignProgress)) {
            case "1":
                strCategory = "History";
                numQuestions = 4;
                break;
            case "2":
                strCategory = "Geography";
                numQuestions = 4;
                break;
            case "3":
                strCategory = "Math";
                numQuestions = 4;
                break;
            case "4":
                strCategory = "Science";
                numQuestions = 4;
                break;
            case "5":
                strCategory = "PUP";
                numQuestions = 4;
                break;
            case "6":
                strCategory = "Captain";
                numQuestions = 4;
                break;

        }

        databaseReference = firebaseDatabase.getReference(strCategory);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int count = (int) dataSnapshot.getChildrenCount();

                //Log.d(StringConfig.LOG_TAG, "count : " + count);

                Integer[] arr = new Integer[count];
                for (int i = 0; i < arr.length; i++) {
                    arr[i] = i;

                }

                Collections.shuffle(Arrays.asList(arr));

                //Log.d(StringConfig.LOG_TAG, "arrayValue : " + Arrays.toString(arr));
                //Log.d(StringConfig.LOG_TAG, "strCategoooooory : " + strCategory);

                for (int i = 0; i < numQuestions; i++) {
                    String index0 = String.valueOf(arr[i]);

                    Log.d(StringConfig.LOG_TAG, "index0 : " + index0);

                    String question = String.valueOf(dataSnapshot.child(index0).child("question").getValue());
                    String correct_answer = String.valueOf(dataSnapshot.child(index0).child("correct_answer").getValue());
                    String incorrect_answer0 = String.valueOf(dataSnapshot.child(index0).child("incorrect_answers").child("0").getValue());
                    String incorrect_answer1 = String.valueOf(dataSnapshot.child(index0).child("incorrect_answers").child("1").getValue());
                    String incorrect_answer2 = String.valueOf(dataSnapshot.child(index0).child("incorrect_answers").child("2").getValue());

                    String encodedQuestion = base64DecoderEncoder.base64Encoder(question);
                    String encodedCorrectAnswer = base64DecoderEncoder.base64Encoder(correct_answer);
                    String encodedIncorrectAnswer0 = base64DecoderEncoder.base64Encoder(incorrect_answer0);
                    String encodedIncorrectAnswer1 = base64DecoderEncoder.base64Encoder(incorrect_answer1);
                    String encodedIncorrectAnswer2 = base64DecoderEncoder.base64Encoder(incorrect_answer2);
                    String encodedCategory = base64DecoderEncoder.base64Encoder(strCategory);
                    String encodedDifficulty = base64DecoderEncoder.base64Encoder("easy");

                   /* Log.d(StringConfig.LOG_TAG, "question : " + question);
                    Log.d(StringConfig.LOG_TAG, "correct_answer : " + correct_answer);
                    Log.d(StringConfig.LOG_TAG, "incorrect_answer0 : " + incorrect_answer0);
                    Log.d(StringConfig.LOG_TAG, "incorrect_answer1 : " + incorrect_answer1);
                    Log.d(StringConfig.LOG_TAG, "incorrect_answer2 : " + incorrect_answer2);*/

                    int dbID = i + 1;
                    //Log.d(StringConfig.LOG_TAG, "dbID : " + dbID);

                    long id = captainSQLiteAdapter.insertData(dbID, encodedQuestion, encodedCorrectAnswer, encodedIncorrectAnswer0, encodedIncorrectAnswer1, encodedIncorrectAnswer2, encodedCategory, encodedDifficulty, "false");
                    //Log.d(StringConfig.LOG_TAG, "idInsertData : " + id);

                    progressDialog.dismiss();

                    navigateToCampaignMain(id, delay);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                progressDialog.dismiss();

                Toast.makeText(CampaignProgressActivity.this, "Error retrieving questions from the databse. ", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void navigateToCampaignMain(long id, int delay) {
        if (id == numQuestions) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    new AnsweredQuestionsManager(CampaignProgressActivity.this).setAnsweredQuestions(1);
                    new CampaignProgressManager(CampaignProgressActivity.this).setAnsweredQuestions(1);

                    Bundle bundle = new Bundle();
                    bundle.putString("category", strCategory);
                    bundle.putInt("numQuestions", numQuestions);

                    Log.d(StringConfig.LOG_TAG, "numQuestions : " + numQuestions);

                    Intent intent = new Intent(CampaignProgressActivity.this, CampaignMainActivity.class);
                    intent.putExtras(bundle);

                    startActivity(intent);
                    finish();

                }
            }, delay);

        }
    }

    private void manageLives(boolean isRight) {

        int currentLives;

        if (!isRight) {

            currentLives = campaignProgressManager.getCampaignLives() - 1;

        } else {

            currentLives = campaignProgressManager.getCampaignLives();

        }

        Log.d(StringConfig.LOG_TAG, "currentLives : " + currentLives);

        campaignProgressManager.setCampaignLives(currentLives);

        switch (campaignProgressManager.getCampaignLives()) {
            case 5:
                iv_life05.setVisibility(View.VISIBLE);
                iv_life04.setVisibility(View.VISIBLE);
                iv_life03.setVisibility(View.VISIBLE);
                iv_life02.setVisibility(View.VISIBLE);
                iv_life01.setVisibility(View.VISIBLE);
                break;
            case 4:
                iv_life05.setVisibility(View.INVISIBLE);
                break;
            case 3:
                iv_life05.setVisibility(View.INVISIBLE);
                iv_life04.setVisibility(View.INVISIBLE);
                break;
            case 2:
                iv_life05.setVisibility(View.INVISIBLE);
                iv_life04.setVisibility(View.INVISIBLE);
                iv_life03.setVisibility(View.INVISIBLE);
                break;
            case 1:
                iv_life05.setVisibility(View.INVISIBLE);
                iv_life04.setVisibility(View.INVISIBLE);
                iv_life03.setVisibility(View.INVISIBLE);
                iv_life02.setVisibility(View.INVISIBLE);
                break;
            case 0:
                iv_life05.setVisibility(View.INVISIBLE);
                iv_life04.setVisibility(View.INVISIBLE);
                iv_life03.setVisibility(View.INVISIBLE);
                iv_life02.setVisibility(View.INVISIBLE);
                iv_life01.setVisibility(View.INVISIBLE);
                break;
        }

    }

    private void manageCategoryBadges() {

        if (campaignProgressManager.getCampaignProgress() != null) {

            String progress = campaignProgressManager.getCampaignProgress();

            switch (progress) {
                case "1":
                    civ_category01.setImageResource(R.drawable.crew_history_defeated);
                    break;
                case "2":
                    civ_category01.setImageResource(R.drawable.crew_history_defeated);
                    civ_category02.setImageResource(R.drawable.crew_geography_defeated);
                    break;
                case "3":
                    civ_category01.setImageResource(R.drawable.crew_history_defeated);
                    civ_category02.setImageResource(R.drawable.crew_geography_defeated);
                    civ_category03.setImageResource(R.drawable.crew_math_defeated);
                    break;
                case "4":
                    civ_category01.setImageResource(R.drawable.crew_history_defeated);
                    civ_category02.setImageResource(R.drawable.crew_geography_defeated);
                    civ_category03.setImageResource(R.drawable.crew_math_defeated);
                    civ_category04.setImageResource(R.drawable.crew_science_defeated);
                    break;
                case "5":
                    civ_category01.setImageResource(R.drawable.crew_history_defeated);
                    civ_category02.setImageResource(R.drawable.crew_geography_defeated);
                    civ_category03.setImageResource(R.drawable.crew_math_defeated);
                    civ_category04.setImageResource(R.drawable.crew_science_defeated);
                    civ_category05.setImageResource(R.drawable.crew_pup_defeated);
                    break;
            }

        } /*else {

            civ_category01.setVisibility(View.INVISIBLE);
            civ_category02.setVisibility(View.INVISIBLE);
            civ_category03.setVisibility(View.INVISIBLE);
            civ_category04.setVisibility(View.INVISIBLE);
            civ_category05.setVisibility(View.INVISIBLE);
        }*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Log.d(StringConfig.LOG_TAG, "campaignProgressActivity:onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Log.d(StringConfig.LOG_TAG, "campaignProgressActivity:onPause called");

    }

    @Override
    public void onBackPressed() {
    }

    public void saveAndExit(View view) {

        int remainingLives = campaignProgressManager.getCampaignLives();
        String progress = campaignProgressManager.getCampaignProgress();

        Log.d(StringConfig.LOG_TAG, "exit:remainingLives : " + remainingLives);
        Log.d(StringConfig.LOG_TAG, "exit:progress : " + progress);

        SaveProgressManager saveProgressManager = new SaveProgressManager(this);
        saveProgressManager.setRemainingLives(remainingLives);
        saveProgressManager.setProgress(progress);

        startActivity(new Intent(CampaignProgressActivity.this, HomeActivity.class));
        finish();


    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
