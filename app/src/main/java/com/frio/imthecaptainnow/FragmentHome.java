package com.frio.imthecaptainnow;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.frio.imthecaptainnow.Config.StringConfig;
import com.frio.imthecaptainnow.SharedPreferencesManager.CategoryManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment {

    View view;
    Button btn_playSolo;
    Button btn_playMultiplayer;
    SwipeRefreshLayout srl_tab_home;

    RequestQueue requestQueue;

    CategoryManager categoryManager;

    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        btn_playSolo = (Button) view.findViewById(R.id.btn_playSolo);
        btn_playMultiplayer = (Button) view.findViewById(R.id.btn_playMultiplayer);
        srl_tab_home = (SwipeRefreshLayout) view.findViewById(R.id.srl_tab_home);

        categoryManager = new CategoryManager(getContext());

        requestQueue = Volley.newRequestQueue(getContext());

        eventListener();
        initOpenTDBApi();

        return view;
    }

    private void eventListener() {

        btn_playSolo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*

                FragmentManager fragmentManager = getFragmentManager();
                DialogFragmentPlay dialogFragmentPlay = new DialogFragmentPlay();
                dialogFragmentPlay.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                dialogFragmentPlay.show(fragmentManager, "tag");
*/

            }
        });

        srl_tab_home.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                srl_tab_home.setRefreshing(true);
                initOpenTDBApi();
                srl_tab_home.setRefreshing(false);
            }
        });

    }

    private void initOpenTDBApi() {

        if ((categoryManager.getCategoryValue("9") == null || categoryManager.getCategoryValue("32") == null)
                || categoryManager.getCategoryValue("9").isEmpty() || categoryManager.getCategoryValue("32").isEmpty()) {

            Log.d(StringConfig.LOG_TAG, "categoryManager.getCategoryValue: " + categoryManager.getCategoryValue("9"));

            getOPENTDBCategories();

        }

    }

    private void getOPENTDBCategories() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Retrieving data from server...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, StringConfig.OPENTDB_URL_RETRIEVE_CATEGORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);

                    Log.d(StringConfig.LOG_TAG, "getOPENTDBCategories:jsonObject : " + jsonObject);

                    JSONArray jsonArray = jsonObject.getJSONArray(StringConfig.OPENTDB_URL_RETRIEVE_CATEGORY_RESPONSE_CATEGORIES);

                    Log.d(StringConfig.LOG_TAG, "getOPENTDBCategories:jsonArray : " + jsonArray);

                    for (int i = 0; i <= jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        int response_id = jsonObject1.getInt(StringConfig.OPENTDB_URL_RETRIEVE_CATEGORY_RESPONSE_ID);
                        String response_name = jsonObject1.getString(StringConfig.OPENTDB_URL_RETRIEVE_CATEGORY_RESPONSE_NAME);

                        Log.d(StringConfig.LOG_TAG, "getOPENTDBCategories:id : " + response_id);
                        Log.d(StringConfig.LOG_TAG, "getOPENTDBCategories:name : " + response_name);

                        categoryManager.setCategoryName(String.valueOf(response_id), response_name);

                        progressDialog.dismiss();


                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                displayToast("Unable to fetch data from server.");

            }
        });

        requestQueue.add(stringRequest);
    }

    private void displayToast(String toastMessage) {
        Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT).show();
    }
}
