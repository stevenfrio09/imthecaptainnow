package com.frio.imthecaptainnow;

import android.util.Log;

import com.frio.imthecaptainnow.Config.StringConfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by steven on 1/5/18.
 */

public class DateTime {

    public String getDateTime() {

        Date date = Calendar.getInstance().getTime();

        Log.d(StringConfig.LOG_TAG, "getDateTime : " + date);


        DateFormat formatter = new SimpleDateFormat("yyyyMMdd'T'HHmm");
        String today = formatter.format(date);

        return today;
    }


}
