package com.frio.imthecaptainnow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContributeQuestionsHomeActivity extends AppCompatActivity {

    Button btn_history, btn_science, btn_math, btn_geography, btn_pup, btn_captain;
    LinearLayout ll_home;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AveriaSerifLibre-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_contribute_questions_home);

        btn_history = findViewById(R.id.btn_history);
        btn_science = findViewById(R.id.btn_science);
        btn_math = findViewById(R.id.btn_math);
        btn_geography = findViewById(R.id.btn_geography);
        btn_pup = findViewById(R.id.btn_pup);
        btn_captain = findViewById(R.id.btn_captainMode);

        ll_home = findViewById(R.id.ll_home);

        snackbar = Snackbar.make(ll_home, "Press back again to exit.", Snackbar.LENGTH_SHORT);


        onClickListener(btn_history);
        onClickListener(btn_science);
        onClickListener(btn_math);
        onClickListener(btn_geography);
        onClickListener(btn_pup);
        onClickListener(btn_captain);


    }

    private void onClickListener(final Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String buttonValue = button.getText().toString();
                Bundle bundle = new Bundle();
                bundle.putString("intentValue", buttonValue);

                Intent intent = new Intent(ContributeQuestionsHomeActivity.this, ContributeQuestionsMainActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(ContributeQuestionsHomeActivity.this, HomeActivity.class));
        finish();

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
