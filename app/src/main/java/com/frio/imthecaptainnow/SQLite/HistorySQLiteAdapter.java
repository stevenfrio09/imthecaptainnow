package com.frio.imthecaptainnow.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by steven on 1/23/18.
 */

public class HistorySQLiteAdapter {

    private HistorySQLite historySQLite;
    private SQLiteDatabase sqLiteDatabase;

    public HistorySQLiteAdapter(Context context) {
        historySQLite = new HistorySQLite(context);
        sqLiteDatabase = historySQLite.getWritableDatabase();
    }

    public void insertData(int correct, int total, String date) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HistorySQLite.COL_CORRECT, correct);
        contentValues.put(HistorySQLite.COL_TOTAL, total);
        contentValues.put(HistorySQLite.COL_DATE, date);

        sqLiteDatabase.insert(HistorySQLite.TABLE_NAME, null, contentValues);

    }


    public static class HistorySQLite extends SQLiteOpenHelper {

        public static final String DATABASE_NAME = "HistorySQLite.db";
        public static final String TABLE_NAME = "history_table";
        public static final String COL_ID = "id";
        public static final String COL_CORRECT = "correct";
        public static final String COL_TOTAL = "total";
        public static final String COL_DATE = "date";
        public static final int DB_VERSION = 1;
        public static final String createQuery = "CREATE TABLE " + TABLE_NAME + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_CORRECT + ", " + COL_TOTAL + ", " + COL_DATE + ")";
        public static final String dropQuery = "DROP TABLE IF EXISTS " + TABLE_NAME + "";


        public HistorySQLite(Context context) {
            super(context, DATABASE_NAME, null, DB_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(createQuery);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(dropQuery);

        }
    }

}
