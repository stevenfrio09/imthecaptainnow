package com.frio.imthecaptainnow.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.frio.imthecaptainnow.Config.StringConfig;

/**
 * Created by steven on 12/21/17.
 */

public class CaptainSQLiteAdapter {

    Context context;
    CaptainSQLite captainSQLite;
    SQLiteDatabase sqLiteDatabase;

    public CaptainSQLiteAdapter(Context context) {
        captainSQLite = new CaptainSQLite(context);
        sqLiteDatabase = captainSQLite.getWritableDatabase();
    }


    public long insertData(int id, String question, String option1, String option2, String option3, String option4, String category, String difficulty, String isAnswered) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CaptainSQLite.COL_ID, id);
        contentValues.put(CaptainSQLite.COL_QUESTION, question);
        contentValues.put(CaptainSQLite.COL_OPTION1, option1);
        contentValues.put(CaptainSQLite.COL_OPTION2, option2);
        contentValues.put(CaptainSQLite.COL_OPTION3, option3);
        contentValues.put(CaptainSQLite.COL_OPTION4, option4);
        contentValues.put(CaptainSQLite.COL_CATEGORY, category);
        contentValues.put(CaptainSQLite.COL_DIFFICULTY, difficulty);
        contentValues.put(CaptainSQLite.COL_ISANSWERED, isAnswered);

        return sqLiteDatabase.insert(CaptainSQLite.TABLE_NAME, null, contentValues);
    }

    /*public String selectQuestion(String id){
        String selectQuery = "SELECT "+CaptainSQLite.COL_QUESTION+" FROM "+CaptainSQLite.COL_OPTION1+" ";
    }*/

    public String selectQuestion(int id) {
        String selectQuery = "SELECT " + CaptainSQLite.COL_QUESTION + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String question = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            question = cursor.getString(0);

        }
        cursor.close();

        return question;

    }

    public String selectOption1(int id) {
        String selectQuery = "SELECT " + CaptainSQLite.COL_OPTION1 + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String option1 = "";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            option1 = cursor.getString(0);

        }
        cursor.close();

        return option1;
    }

    public String selectOption2(int id) {
        String selectQuery = "SELECT " + CaptainSQLite.COL_OPTION2 + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String option2 = "";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            option2 = cursor.getString(0);
        }
        cursor.close();

        return option2;
    }


    public String selectOption3(int id) {

        String selectQuery = "SELECT " + CaptainSQLite.COL_OPTION3 + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String option3 = "";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            option3 = cursor.getString(0);
        }
        cursor.close();

        return option3;
    }

    public String selectOption4(int id) {

        String selectQuery = "SELECT " + CaptainSQLite.COL_OPTION4 + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String option4 = "";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            option4 = cursor.getString(0);
        }
        cursor.close();

        return option4;
    }

    public String selectCategory(int id) {
        String selectQuery = "SELECT " + CaptainSQLite.COL_CATEGORY + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String category = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            category = cursor.getString(0);
        }

        cursor.close();

        return category;

    }

    public String selectDifficulty(int id) {
        String selectQuery = "SELECT " + CaptainSQLite.COL_DIFFICULTY + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String difficulty = "";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            difficulty = cursor.getString(0);

        }

        cursor.close();

        return difficulty;

    }

    public String selectIsAnswered(int id) {
        String selectQuery = "SELECT " + CaptainSQLite.COL_ISANSWERED + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";

        String isAnswered = "";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            isAnswered = cursor.getString(0);

        }

        cursor.close();

        return isAnswered;

    }

    public void updateIsAnswered(int id) {

        sqLiteDatabase = captainSQLite.getWritableDatabase();
        String updateQuery = "UPDATE " + CaptainSQLite.TABLE_NAME + " SET " + CaptainSQLite.COL_ISANSWERED + " = '" + "true" + "' WHERE " + CaptainSQLite.COL_ID + " = '" + id + "'";
        sqLiteDatabase.execSQL(updateQuery);
    }

    public String selectId(String question) {
        String selectQuery = "SELECT " + CaptainSQLite.COL_ID + " FROM " + CaptainSQLite.TABLE_NAME + " WHERE " + CaptainSQLite.COL_QUESTION + " = '" + question + "'";

        String id = "";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            id = cursor.getString(0);
        }

        cursor.close();

        return id;

    }


    public void deleteContents(){
        String deleteQuery = "DELETE FROM "+CaptainSQLite.TABLE_NAME+"";
        sqLiteDatabase.execSQL(deleteQuery);
        Log.d(StringConfig.LOG_TAG, "delete query called");

    }


    public static class CaptainSQLite extends SQLiteOpenHelper {

        static final String DATABASE_NAME = "CaptainSQLite.db";
        static final String TABLE_NAME = "CaptainSQLiteTable";
        static final String COL_ID = "ID";
        static final String COL_QUESTION = "Question";
        static final String COL_OPTION1 = "Option1";
        static final String COL_OPTION2 = "Option2";
        static final String COL_OPTION3 = "Option3";
        static final String COL_OPTION4 = "Option4";
        static final String COL_CATEGORY = "Category";
        static final String COL_DIFFICULTY = "Difficulty";
        static final String COL_ISANSWERED = "isAnswered";
        static final int DATABASE_VERSION = 1;

        static final String CREATE_QUERY = "CREATE TABLE " + TABLE_NAME + "(" + COL_ID + " INTEGER PRIMARY KEY, " + COL_QUESTION + ", " + COL_OPTION1 + ", " + COL_OPTION2 + ", " + COL_OPTION3 + ", " + COL_OPTION4 + ", " + COL_CATEGORY + ", " + COL_DIFFICULTY + ", " + COL_ISANSWERED + ")";
        static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME + "";

        public CaptainSQLite(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_QUERY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_QUERY);
        }
    }

}
